<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ThemeSettings */

$this->title = 'Настройки темы';
$this->params['breadcrumbs'][] = ['label' => 'Настройки темы', 'url' => ['index']];
?>
<div class="theme-settings-view">

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'times:ntext',
            'email:email',
            'adress:ntext',
            'vk',
            'fb',
            'tw',
            'odn',
            //'main_photo',
            [
                'attribute'=>'main_photo',
                'value'=>'../../frontend/web/mt/img/'.$model->main_photo,
                'format' => ['image',['width'=>'60px']],
            ],
            'main_title',
            'main_text:ntext',
            'about_page:ntext',
            'delivery_page:ntext',
            'contactus_page_title',
            'contactus_page_text:ntext',
            'contactus_page_map:ntext',
            'return_page:ntext',
            'seo_main_title',
            'seo_main_keys:ntext',
            'seo_main_desc:ntext',
            'seo_about_title',
            'seo_about_keys:ntext',
            'seo_about_desc:ntext',
            'seo_delivery_title',
            'seo_delivery_keys:ntext',
            'seo_delivery_desc:ntext',
            'seo_contacts_title',
            'seo_contacts_keys:ntext',
            'seo_contacts_desc:ntext',
            'seo_return_title',
            'seo_return_keys:ntext',
            'seo_return_desc:ntext',
        ],
    ]) ?>

</div>
