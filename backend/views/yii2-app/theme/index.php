<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Theme Settings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="theme-settings-index">

    <p>
        <?= Html::a('Создать настройки темы', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],
            //'id',
            'times:ntext',
            'email:email',
            'adress:ntext',
            // 'vk',
            // 'fb',
            // 'tw',
            // 'odn',
            // 'main_photo',
            // 'main_title',
            // 'main_text:ntext',
            // 'about_page:ntext',
            // 'delivery_page:ntext',
            // 'contactus_page_title',
            // 'contactus_page_text:ntext',
            // 'contactus_page_map:ntext',
            // 'seo_main_title',
            // 'seo_main_keys:ntext',
            // 'seo_main_desc:ntext',
            // 'seo_about_title',
            // 'seo_about_keys:ntext',
            // 'seo_about_desc:ntext',
            // 'seo_delivery_title',
            // 'seo_delivery_keys:ntext',
            // 'seo_delivery_desc:ntext',
            // 'seo_contacts_title',
            // 'seo_contacts_keys:ntext',
            // 'seo_contacts_desc:ntext',


        ],
    ]); ?>
<?php Pjax::end(); ?></div>
