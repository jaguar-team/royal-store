<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\ThemeSettings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="theme-settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'shop_name')->textInput()  ?>

    <?= $form->field($model, 'shop_url')->textInput()  ?>

    <?= $form->field($model, 'times')->textInput() ?>

    <?= $form->field($model, 'phone1')->textInput()  ?>

    <?= $form->field($model, 'phone2')->textInput()  ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'adress')->textInput() ?>

    <?= $form->field($model, 'vk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fb')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tw')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'odn')->textInput(['maxlength' => true]) ?>

   <?php if($model->main_photo){echo "<img style='width:100px;' src='../../frontend/web/mt/img/".$model->main_photo."' alt='ph' />";}?>
    <?= $form->field($model, 'main_photo')->fileInput();?>
    
    <?= $form->field($model, 'main_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'main_text')->textarea(['rows' => 4])   ?>

    <?= $form->field($model, 'about_page')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full'
        ])  ?>

    <?= $form->field($model, 'delivery_page')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full'
        ])  ?>

    <?= $form->field($model, 'contactus_page_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contactus_page_text')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full'
        ])  ?>

    <?= $form->field($model, 'contactus_page_map')->textarea(['rows' => 6]) ?>
    
    <?= $form->field($model, 'return_page')->widget(CKEditor::className(), [
    'options' => ['rows' => 6],
    'preset' => 'full'
    ])  ?>
    
    <h3 style="color: #3C8DBC;">SEO</h3>

    <?= $form->field($model, 'seo_main_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'seo_main_keys')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'seo_main_desc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'seo_about_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'seo_about_keys')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'seo_about_desc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'seo_delivery_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'seo_delivery_keys')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'seo_delivery_desc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'seo_contacts_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'seo_contacts_keys')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'seo_contacts_desc')->textarea(['rows' => 6]) ?>
    
    <?= $form->field($model, 'seo_return_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'seo_return_keys')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'seo_return_desc')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
