<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ThemeSettings */

$this->title = 'Create Theme Settings';
$this->params['breadcrumbs'][] = ['label' => 'Theme Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="theme-settings-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
