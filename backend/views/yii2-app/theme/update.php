<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ThemeSettings */

$this->title = 'Редактировать Настройки темы: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Настройки темы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="theme-settings-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
