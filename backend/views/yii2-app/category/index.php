<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel common\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать категорию', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'format' => 'text',
                'filter' => false,
            ],
            [
                'attribute' => 'cat_img',
                'format' => ['image',['width'=>'60px']],
                'filter' => false,
                'value' => function($data) {
                    return ($data->cat_img ? '../../frontend/web/mt/img/'.$data->cat_img : NULL);
                },
            ],
            [
                'attribute' => 'name',
                'format' => 'text',
                'filter' => false,
            ],
            [
                'attribute' => 'parent',
                'value' => function($data) {
                    return ($data->parent ? $data->getCategoryNameById($data->parent) : NULL);
                },
            ],
            'position',
            //'alias',
            //'seo_title',
            // 'seo_keys:ntext',
            // 'seo_desc:ntext',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
