<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use dosamigos\ckeditor\CKEditor;
use common\models\Category;

/* @var $this yii\web\View */
/* @var $model common\models\Category */
/* @var $form yii\widgets\ActiveForm */

if (!$model->isNewRecord) {
    $categories = Category::find()->select(['name', 'id'])->where('id != ' . $model->id)->indexBy('id')->column();
}
else
    $categories = Category::find()->select(['name', 'id'])->indexBy('id')->column();

$categories[0] = '--';
asort($categories);

?>

<div class="category-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php //= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

    <?php 
    if($model->cat_img){
        echo "<img style='width:15px;' src='../../frontend/web/mt/img/".$model->cat_img."' alt='ph' />";
    }
    
    echo $form->field($model, 'cat_img')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => ['previewFileType' => 'image','showUpload' => false]
    ]); ?>

    <?php
    if($model->banner_img){
        echo "<img style='width:100px;' src='../../frontend/web/mt/img/".$model->banner_img."' alt='' />";
        echo '<a href='.Url::toRoute(['category/deletebanner', 'id'=>$model->id]).'><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>';
    }?>
    <?= $form->field($model, 'banner_img')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => ['previewFileType' => 'image','showUpload' => false]
    ]); ?>

    <?= $form->field($model, 'banner_link')->textInput() ?>

    <?= $form->field($model, 'position')->textInput() ?>

    <?php if ($categories): ?>
        <?= $form->field($model, 'parent')->dropDownList($categories); ?>
    <?php endif; ?>

    <?= $form->field($model, 'desc')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full'
    ]); ?>
    <?= $form->field($model, 'seo_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'seo_keys')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'seo_desc')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
