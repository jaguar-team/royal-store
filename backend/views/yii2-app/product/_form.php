<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\widgets\Pjax;
use common\models\Category;
use dosamigos\ckeditor\CKEditor;
use common\models\Size;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\widgets\ActiveForm */

?>
<style>
    .clear
    {
        clear: both;
    }
    .ph{
        float: left;
        position: relative;
    }
    .ph a
    {
        position: absolute;
        top: 0;
        right: 0;
        background-color: #fff;
        border: 1px solid #000;
    }
</style>
<?php Pjax::begin();?>
<div class="product-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'position')->textInput() ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <!-- categories -->
    <?php
        $categories = Category::find()->select(['name','id'])->indexBy('id')->column();
        $categories[0] = '--';
        $params = [
            //'prompt' => 'Выберите статус...',
            //'multiple' => 'true'
        ];
        asort($categories);
    ?>
    <?= $form->field($model, 'category_id')->dropDownList($categories); ?>
    <!-- /categories -->

    <!-- attributes -->
    <div class="attribute">
        <h4>Атрибуты товара</h4>
        <ul>
            <?php foreach ($model->productAttributeModel->getAll(false) as $number => $attribute): ?>
                <li>
                    <?php
                        $attributeValues = $attribute->productAttributeValueModel->find()->select(['value', 'id'])->where(['id_attribute' => $attribute->id])->indexBy('id')->column();

                        foreach ($attributeValues as $id => $value) {
                            $attributeValues[$attribute->id.','.$id] = $value;
                            unset($attributeValues[$id]);
                        }
                        $attributeValues['false'] = '--';
                        asort($attributeValues);
                    ?>
                    <?= $form->field($model, 'attributes[]')->dropDownList($attributeValues)->label($attribute->name); ?>
                </li>
            <?php endforeach; ?>
            <li><?= Html::a('Добавить новый аттрибут', ['product-attribute/create']); ?></li>
        </ul>
    </div>
    <!-- /attributes -->
    <style>
        .attribute ul {
            padding: 0;
        }
        .attribute ul > li{
            display: inline-block;
        }
    </style>
    
    <?= $form->field($model, 'top')->dropDownList([1=>'В топ товарах',0=>'Не в топ товарах']) ?>

     <?= $form->field($model, 'featured')->dropDownList([1=>'В рекомендуемых',0=>'Не в рекомендуемых']) ?>

     <?= $form->field($model, 'news')->dropDownList([1=>'В новинках',0=>'Не в новинках']) ?>

    <?= $form->field($model, 'amount')->dropDownList([1=>'В наличии',0=>'Не в наличии']) ?>
   
   <?= $form->field($model, 'made')->textInput() ?>

    <?= $form->field($model, 'sizes')->widget(Select2::className(), [
        'data' => Size::find()->select(['name'])->indexBy('name')->column(),
        'size' => Select2::MEDIUM,
        'options' => ['placeholder' => 'Выберите размеры...', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true,
        ],
        ]) ?>
    
    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'discount')->textInput() ?>
    
    <?php if($model->main_img){echo "<img style='width:100px;' src='../../frontend/web/mt/img/".$model->main_img."' alt='ph' />";}?>
    <?= $form->field($model, 'main_img')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => ['previewFileType' => 'image','showUpload' => false]
    ]); ?>
    
    <?php if($model->photos){
        $photos=explode("##", $model->photos);
        foreach ($photos as $photo){?>
            <?php if($photo){ ?>
            <div class="ph">
                <img style='width:100px;' src='../../frontend/web/mt/img/<?=$photo;?>'/>
                <a href="<?=Url::toRoute(['product/deletephoto', 'id'=>$model->id, 'name'=>trim($photo)]);?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
            </div>
            <?php }?>
    <?php } }?>
    <div class="clear"></div>
    <?= $form->field($model, 'photos[]')/*->fileInput(['multiple' => true, 'accept' => 'image/*']); */->widget(FileInput::classname(), [
        'options' => ['multiple' => true, 'accept' => 'image/*'],
        'pluginOptions' => ['previewFileType' => 'image','showUpload' => false,]
    ]); ?>

    <?= $form->field($model, 'desc')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full'
        ])   ?>

    <?= $form->field($model, 'about_brand')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full'
        ])   ?>

    <?= $form->field($model, 'seo_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'seo_keys')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'seo_desc')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Запомнить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php Pjax::end();?>
