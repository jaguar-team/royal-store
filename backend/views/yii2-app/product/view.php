<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$arr=[0=>'Нет в наличии', 1=>'В наличии'];
$top=[0=>'Не в топ товарах', 1=>'В топ товарах'];
$featured=[0=>'Не в рекомендуемых', 1=>'В рекомендуемых'];
$news=[0=>'Не в новинках', 1=>'В новинках'];
?>
<style>
    .clear
    {
        clear: both;
    }
    .ph{
        float: left;
        position: relative;
    }
</style>
<div class="product-view">


    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот товар?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',
            'position',
            'code',
            [
                'attribute'=>'category_id',
                'value'=> ArrayHelper::getValue($model, 'category.name')
            ],
            //'amount',
            [
                'attribute'=>'amount',
                'value'=>$arr[$model->amount]
            ],
            //'top',
            [
                'attribute'=>'top',
                'value'=>$top[$model->top]
            ],
            //'featured',
            [
                'attribute'=>'featured',
                'value'=>$featured[$model->featured]
            ],
            //'news',
            [
                'attribute'=>'news',
                'value'=>$news[$model->news]
            ],
            'sizes',
            'made',
            'price',
            'discount',
            //'main_img',
            [
                'label' => 'Картинка',
                'attribute'=>'main_img',
                'value'=>'../../frontend/web/mt/img/'.$model->main_img,
                'format' => ['image',['width'=>'60px']],
            ],
            //'photos:ntext',
            'desc:ntext',
            'about_brand:ntext',
            //'category_id',
            'seo_title',
            'seo_keys:ntext',
            'seo_desc:ntext',
            'alias',
        ],
    ]) ?>
    <div class="row">
        <div class="col-md-3">
            <h4>Картинки</h4>
        </div>
        <div class="col-md-10">
             <?php $photos=explode("##", $model->photos);
            foreach ($photos as $photo){?>
            <?php if($photo){ ?>
            <div class="ph">
                <img style='width:100px;' src='../../frontend/web/mt/img/<?=$photo;?>'/>
            </div>
            <?php }?>
            <?php }?>
        </div>
        <div class="clear"></div>
    </div>
</div>
