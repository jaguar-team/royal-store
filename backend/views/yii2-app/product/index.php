<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use common\models\Category;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $searchModel common\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Товары';
$this->params['breadcrumbs'][] = $this->title;
$arr=[0=>'Нет в наличии', 1=>'В наличии'];
?>
<div class="product-index">
    <p>
        <?= Html::a('Добавить Товар', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin();?>
    <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($product, 'increase_price')->textInput(['placeholder' => 'Введите фиксированную цену в грн.']) ?>
        <?= $form->field($product, 'decrease_price')->textInput(['placeholder' => 'Введите фиксированную цену в грн.']) ?>
        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']); ?>
        </div>
    <?php ActiveForm::end(); ?>
    <?php Pjax::end();?>
    
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],
            'name',
            [
                'attribute' => 'price',
                'format' => 'text',
                'content' => function($data){
                    $string = '<form method="post" action="">';
                    $string .= '<input name="Product[id]" class="form-control" type="hidden" value="'.$data->id.'" />';
                    $string .= '<input name="Product[price]" class="form-control" type="text" value="'.$data->price.'" />';
                    $string .= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'style' => 'margin-top: 5px;']);
                    $string .= '</form>';
                    return $string;
                }

            ],
            [
                'attribute'=>'category_id',
                'filter'=>Category::find()->select(['name','id'])->indexBy('id')->column(),
                'value'=>'category.name'
            ],
            'position',
            'code',
            //'amount',
            [
                'filter'=>[0=>'Нет в наличии', 1=>'В наличии'],
                'attribute'=>'amount',
                'format' => 'raw',
                'value' => function($data){
                //return $arr[$data->amount];
                    if($data->amount==1)
                        return "В наличии";
                    else
                        return "Нет в наличии";
                }
            ],
            // top
            [
                'filter'=>[0=>'Не в топ товарах', 1=>'В топ товарах'],
                'attribute'=>'top',
                'format' => 'raw',
                'value' => function($data){
                    if($data->top==1)
                        return "<i class=\"fa fa-check\" aria-hidden=\"true\"></i>";
                    else
                        return "<i class=\"fa fa-times\" aria-hidden=\"true\"></i>";
                }
            ],
            // featured
            [
                'filter'=>[0=>'Не в новынках', 1=>'В новинках'],
                'attribute'=>'featured',
                'format' => 'raw',
                'value' => function($data){
                    if($data->news==1)
                        return "<i class=\"fa fa-check\" aria-hidden=\"true\"></i>";
                    else
                        return "<i class=\"fa fa-times\" aria-hidden=\"true\"></i>";
                }
            ],

[
                'filter'=>[0=>'Не в новынках', 1=>'В новинках'],
                'attribute'=>'news',
                'format' => 'raw',
                'value' => function($data){
                    if($data->news==1)
                        return "<i class=\"fa fa-check\" aria-hidden=\"true\"></i>";
                    else
                        return "<i class=\"fa fa-times\" aria-hidden=\"true\"></i>";
                }
            ],
            // 'discount',
            // 'main_img',
            // 'photos:ntext',
            // 'desc:ntext',
            // 'seo_keys:n',
            // 'about_brand:ntext',
            // 'category_id',
            // 'seo_title',text',
            // 'seo_desc:ntext',
            // 'alias',


        ],
    ]); ?>
<?php Pjax::end(); ?></div>
