<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\Product;
$this->title="Новые заказы";
?>
<?php if(!empty($orders)){?>
<div class="container">
    <div class="row">
        <?php  foreach($orders as $order){?>
        <div class="col-md-5">
            <ul class="list-group">
              <li class="list-group-item">
                <div>
                    <span>Доставленно (добавить в архив): </span><a href="<?=Url::toRoute(['site/addtoarchive', 'id'=>$order->id]);?>"><span class="label label-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></span></a>
                    <span style="margin-left:20px">Удалить: </span>
                    <?= Html::a('<span class="label label-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></span>', ['site/delete', 'id'=>$order->id], [
                        'data' => [
                            'confirm' => 'Вы уверенны, что хотите удалить этот заказ?',
                        ],
                    ]) ?>
                </div>
              </li>
              <li class="list-group-item"><b>id: </b><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span> <?=$order->id;?></li>
              <li class="list-group-item"><b>Имя: </b><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span> <?=$order->name;?></li>
              <li class="list-group-item"><b>Телефон: </b><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span> <?=$order->phone;?></li>
              <li class="list-group-item"><b>Город: </b><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span> <?=$order->city;?></li>
              <li class="list-group-item"><b>Новая почта: </b><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span> <?=$order->office;?></li>
              <li class="list-group-item"><b>Общая сумма: </b><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span> <?=$order->total;?></li>
              <li class="list-group-item">
                <table class="table table-striped" style="border: 1px solid #000;">
                    <tr>
                        <th>Товар</th>
                        <th>Код товара</th>
                        <th>Количество</th>
                        <th>Размер</th>
                    </tr>
                    <?php $p_ids=explode("##", $order->prod_ids);
                        $products=(new Product)->findProductsByOrderIds($p_ids);
                        $count_ids=explode("##",$order->prod_counts);
                        $size_ids=explode("##",$order->prod_sizes);
                        $i=0; foreach($products as $prod){
                    ?>
                    <tr>
                        <td><a href="<?=Url::toRoute(['/../main/product', 'alias'=>$prod['alias']]);?>"><?=$prod['name'];?></a><?php if (!$prod['name']) echo 'товар удален';?></td>
                        <td><?=$prod['code'];?></td>
                        <td><?=$count_ids[$i];?></td>
                        <td><?=$size_ids[$i];?></td>
                    </tr>
                    <?php $i++;}?>
                </table>
              </li>
            </ul>
        </div>
        <?php }?>
    </div>
    <a style="width:83%;" class="btn btn-success" href="<?=Url::toRoute(['site/orders']);?>">Все заказы</a>
</div>
<?php } else {?>
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-warning" style="text-align:center;" role="alert">Новых заказов пока что нету...</div>
        <a style="width:100%;" class="btn btn-success" href="<?=Url::toRoute(['site/orders']);?>">Все необработанные заказы</a>
    </div>
    </div>
<?php }?>