<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

$this->title = 'Комментарии';
?>
<?php Pjax::begin(); ?>    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id_product',
                'format' => 'text',
                'filter' => false,
                'value' => function($data) {
                    return $data->getProductById()->name;
                },
            ],
            [
                'attribute' => 'name',
                'format' => 'text',
                'filter' => false,
                /**'value' => function($data) {
                    return ($data->cat_img ? '../../frontend/web/mt/img/'.$data->cat_img : NULL);
                },**/
            ],
            [
                'attribute' => 'comment',
                'format' => 'text',
                'filter' => false,
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?>