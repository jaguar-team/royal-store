<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\widgets\Pjax;
use common\models\Category;
use dosamigos\ckeditor\CKEditor;
use common\models\Size;

$this->title = 'Фильтры';

$attributes = $product_attribute_model->find()->select(['name', 'id'])->where([])->indexBy('id')->column();
//exit();
?>
<div class="product-filter">
    <?php Pjax::begin();?>
        <?php $form = ActiveForm::begin(); ?>
            <?=
                $form->field($model, 'f_general')->checkboxList([
                    'price'     => 'Цене',
                    'sub_cat'   => 'Подкатегориям',
                ])->label('Фильтровать по:');
            ?>

            <!-- attributes -->
            <?= $form->field($model, 'f_attr')->checkboxList($attributes)->label('Фильтровать по атрибутам:'); ?>
            <!-- /attributes -->

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Запомнить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    <?php Pjax::end();?>
</div>
