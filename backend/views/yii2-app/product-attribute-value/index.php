<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use common\models\Category;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $searchModel common\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Значения';
$this->params['breadcrumbs'][] = $this->title;
$arr=[0=>'Нет в наличии', 1=>'В наличии'];
?>
<div class="product-index">

    <p>
        <?= Html::a('Добавить значение', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Добавить атрибут', ['product-attribute/create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'value',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?></div>
