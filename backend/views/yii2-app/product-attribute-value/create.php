<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = 'Добавить новое значение для атрибута';
$this->params['breadcrumbs'][] = ['label' => 'Значения', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$attributes = [];

foreach ($attributes_model as $number => $attribute) {
    $attributes[$attribute->id] = $attribute->name;
}
?>
<div class="product-create">


    <?= $this->render('_form', [
        'model'         => $model,
        'attributes'    => $attributes,
    ]) ?>

</div>
