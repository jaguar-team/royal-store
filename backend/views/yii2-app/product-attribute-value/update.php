<?php

use yii\helpers\Html;


$this->title = 'Редактирование значения ';
$this->params['breadcrumbs'][] = ['label' => 'Значения', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';

$attributes = [];

foreach ($attributes_model as $number => $attribute) {
    $attributes[$attribute->id] = $attribute->name;
}
?>
<div class="product-update">

    <?= $this->render('_form', [
        'model'         => $model,
        'attributes'    => $attributes,
    ]) ?>

</div>
