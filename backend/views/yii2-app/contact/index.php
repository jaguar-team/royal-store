<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сообщения';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{delete}{link}',
            ],
            //'id',
            'name',
            'phone',
            'text:ntext',
            //'date',
            [
                'attribute'=>'date',
                'format' => 'raw',
                'value' => function($data){
                    return date('d.m.Y', $data->date);
                }
            ],
        ],
    ]); ?>
</div>
