<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Size */

$this->title = 'Редактировать слайд';
$this->params['breadcrumbs'][] = ['label' => 'Размеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="size-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
