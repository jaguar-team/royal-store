<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use common\models\Category;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $searchModel common\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Атрибуты и комбинации';
$this->params['breadcrumbs'][] = $this->title;
$arr=[0=>'Нет в наличии', 1=>'В наличии'];
?>
<div class="product-index">

    <p>
        <?= Html::a('Добавить атрибут', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Добавить значение', ['product-attribute-value/create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'name',
            [
                'attribute' => 'quantity',
                'value'     => function($data) {
                    return $data->getQuantityValue();
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update} {delete}',
                'buttons' => [
                    'view' => function ($url,$model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['product-attribute-value/index', 'id' => $model->id]);
                    },
                ],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?></div>
