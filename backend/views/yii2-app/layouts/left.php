<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>admin</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Меню', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Каталог',
                        'icon' => 'fa fa-book',
                        'url' => ['/product'],
                        'items' => [
                            ['label' => 'Товары',  'url' => ['/product'],],
                            ['label' => 'Категории',  'url' => ['/category'],],
                            ['label' => 'Атрибуты и комбинации',  'url' => ['/product-attribute'],],
                            ['label' => 'Фильтры',  'url' => ['/product-filter'],],
                            ['label' => 'Размеры',  'url' => ['/size/index'],],
                        ],
                    ],
                    /*['label' => 'Размеры', 'icon' => 'fa fa-tags', 'url' => ['/size'],],*/
                    [
                        'label' => 'Заказы', 
                        'icon' => 'fa fa-dashboard', 
                        'url' => ['#'],
                        'items' => [
                            ['label' => 'Необработанные', 'icon' => 'fa fa-file-code-o', 'url' => ['/site/orders'],],
                            ['label' => 'Обработанные', 'icon' => 'fa fa-dashboard', 'url' => ['/site/archive'],],
                        ],
                    ],
                    ['label' => 'Сообщения', 'icon' => 'fa fa-envelope', 'url' => ['/contact'],],
                    ['label' => 'Настройки темы', 'icon' => 'fa fa-cog', 'url' => ['/theme/'],],
                    ['label' => 'Слайдер', 'icon' => 'fa fa-picture-o', 'url' => ['/slider'],],
                    ['label' => 'Комментарии', 'icon' => 'fa fa-comment-o', 'url' => ['/product-comment/index'],],
                ],
            ]
        ) ?>

    </section>

</aside>
