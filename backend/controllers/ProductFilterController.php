<?php

namespace backend\controllers;

use Yii;
use common\models\Category;
use common\models\CategorySearch;
use common\models\ProductFilter;
use common\models\ProductAttribute;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\components\Translit;
use yii\filters\AccessControl;


class ProductFilterController extends MyController
{
    public function actionIndex()
    {
        $filterModel = new ProductFilter();
        $productAttributeModel = new ProductAttribute();

        if ($filterModel->load(Yii::$app->request->post())) {

            if ($filterModel->f_general) {
                $filter_1 = new ProductFilter();
                $object = $filter_1->find()->where(['type' => 'general'])->one();

                if ($object) {
                    $object->type = 'general';
                    $object->value = json_encode($filterModel->f_general);
                    $object->save();
                } else {
                    $filter_1->type = 'general';
                    $filter_1->value = json_encode($filterModel->f_general);
                    $filter_1->save();
                }
            }

            if ($filterModel->f_attr) {
                $filter_2 = new ProductFilter();

                $object_1 = $filter_2->find()->where(['type' => 'attr'])->one();

                if ($object_1) {
                    $object_1->type = 'attr';
                    $object_1->value = json_encode($filterModel->f_attr);
                    $object_1->save();
                } else {
                    $filter_2->type = 'attr';
                    $filter_2->value = json_encode($filterModel->f_attr);
                    $filter_2->save();
                }
            }
        }

        return $this->render('index', [
            'model'                     => $filterModel,
            'product_attribute_model'   => $productAttributeModel,
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}