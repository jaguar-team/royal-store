<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\Order;
use yii\data\Pagination;
use backend\models\PasswordResetRequestForm;
use backend\models\ResetPasswordForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'request-password-reset', 'reset-password'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'orders', 'addtoarchive', 'delete', 'archive'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $last_id=Yii::$app->request->cookies->getValue('last_id');
        if(!$last_id)
            $last_id=0;
        $orders=Order::find()->orderBy('id DESC')->where(['archive'=>NULL])->andWhere(['>', 'id',$last_id])->limit(40)->all();
        return $this->render('index', [
            'orders'=>$orders
        ]);
    }


    public function actionOrders()
    {
        $orders=Order::find()->orderBy('id DESC')->where(['archive'=>NULL])->all();
        Yii::$app->response->cookies->add(new \yii\web\Cookie([
            'name' => 'last_id',
            'value' => $orders[0]->id,
        ]));
        return $this->render('orders',[
            'orders'=>$orders
        ]);
    }
    public function actionArchive()
    {
        $query=Order::find()->orderBy('id DESC')->where(['archive'=>1]);
        $pagination = new Pagination([
                'defaultPageSize'=>2,
                'totalCount'=>$query->count()
            ]);
        $orders=$query->offset($pagination->offset)->limit($pagination->limit)->all();
        return $this->render('archive',[
            'orders'=>$orders,
            'pagination'=>$pagination,
        ]);
    }
    public function actionAddtoarchive($id)
    {
        $order=Order::findOne(['id'=>$id]);
        $order->archive=1;
        $order->save();
        return $this->redirect(Yii::$app->request->referrer);
    }
    public function actionDelete($id)
    {
        $order=Order::findOne($id);
        $order->delete();
        return $this->redirect(Yii::$app->request->referrer);
    }
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    
    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $this->layout = 'main-login';
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Проверьте ваш почтовый ящик для продолжения.');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Вы ошиблись при наборе почты.');
            }
        }
        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }
    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        $this->layout = 'main-login';
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Новый пароль установлен.');
            return $this->goHome();
        }
        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
