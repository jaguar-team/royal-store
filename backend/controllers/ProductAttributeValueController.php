<?php
/**
 * Created by PhpStorm.
 * User: vlad.dneprov
 * Date: 08.02.2017
 * Time: 12:08
 */

namespace backend\controllers;

use Yii;
use common\models\ProductAttribute;
use common\models\ProductAttributeValue;
use common\models\ProductAttributeValueSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\components\Translit;

class ProductAttributeValueController extends MyController
{
    public function actionIndex($id)
    {
        $searchModel = new ProductAttributeValueSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        return $this->render('index', [
            'dataProvider'  => $dataProvider,
            'searchModel'   => $searchModel,
        ]);
    }

    public function actionView($id)
    {
    }

    public function actionCreate()
    {
        $attributeValueModel = new ProductAttributeValue();
        $attributeModel = new ProductAttribute();

        $attributes_model = $attributeModel->find()->all();

        if ($attributeValueModel->load(Yii::$app->request->post())) {
            if ($attributeValueModel->save())
                return $this->redirect(['index', 'id' => $attributeValueModel->id_attribute]);
        }

        return $this->render('create', [
            'model'                     => $attributeValueModel,
            'attributes_model'          => $attributes_model,
        ]);
    }

    public function actionUpdate($id)
    {
        $attributeValueModel = $this->findModel($id);
        $attributeModel = new ProductAttribute();

        $attributes_model = $attributeModel->find()->all();

        if ($attributeValueModel->load(Yii::$app->request->post())) {
            if ($attributeValueModel->save())
                return $this->redirect(['index', 'id' => $attributeValueModel->id_attribute]);
        }

        return $this->render('update', [
            'model'                     => $attributeValueModel,
            'attributes_model'          => $attributes_model,
        ]);
    }

    public function actionDelete($id)
    {
        $attributeValueModel = $this->findModel($id);

        $id_attribute = $attributeValueModel->id_attribute;

        $attributeValueModel->delete();

        return $this->redirect(['index', 'id' => $id_attribute]);
    }

    protected function findModel($id)
    {
        if (($model = ProductAttributeValue::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}