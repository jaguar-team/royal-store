<?php
/**
 * Created by PhpStorm.
 * User: vlad.dneprov
 * Date: 08.02.2017
 * Time: 12:08
 */

namespace backend\controllers;

use Yii;
use common\models\ProductAttribute;
use common\models\ProductAttributeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\components\Translit;

class ProductAttributeController extends MyController
{
    public function actionIndex()
    {
        $searchModel = new ProductAttributeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionView($id)
    {
    }

    public function actionCreate()
    {
        $attributeModel = new ProductAttribute();

        if ($attributeModel->load(Yii::$app->request->post())) {
            if ($attributeModel->save())
                return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $attributeModel,
        ]);
    }

    public function actionUpdate($id)
    {
        $attributeModel = $this->findModel($id);

        if ($attributeModel->load(Yii::$app->request->post())) {
            if ($attributeModel->save())
                return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $attributeModel,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = ProductAttribute::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}