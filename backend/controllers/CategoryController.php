<?php

namespace backend\controllers;

use Yii;
use common\models\Category;
use common\models\CategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\components\Translit;
use yii\filters\AccessControl;
use yii\imagine\Image;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends MyController
{
    /**
     * @inheritdoc
     */
    /*public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }*/

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Category model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Category();

        if ($model->load(Yii::$app->request->post())) {
            $model->alias=(new Translit())->str2url($model->name);
            if(UploadedFile::getInstance($model, 'cat_img'))
            {
                $model->cat_img=UploadedFile::getInstance($model, 'cat_img');
                $model->cat_img->saveAs('../../frontend/web/mt/img/'.$model->cat_img->baseName.".".$model->cat_img->extension);
                Image::thumbnail('../../frontend/web/mt/img/'.$model->cat_img->baseName.".".$model->cat_img->extension, 350, 365)->save(Yii::getAlias('../../frontend/web/mt/img/'.$model->cat_img->baseName.".".$model->cat_img->extension), ['quality' => 50]);
                $model->cat_img=$model->cat_img->baseName.".".$model->cat_img->extension;   
            }
            if(UploadedFile::getInstance($model, 'banner_img'))
            {
                $model->banner_img=UploadedFile::getInstance($model, 'banner_img');
                $model->banner_img->saveAs('../../frontend/web/mt/img/'.$model->banner_img->baseName.".".$model->banner_img->extension);
                Image::thumbnail('../../frontend/web/mt/img/'.$model->banner_img->baseName.".".$model->banner_img->extension, 1140, 350)->save(Yii::getAlias('../../frontend/web/mt/img/'.$model->banner_img->baseName.".".$model->banner_img->extension), ['quality' => 50]);
                $model->banner_img=$model->banner_img->baseName.".".$model->banner_img->extension;
            }
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $img=$model->cat_img;
        $img2=$model->banner_img;
        if ($model->load(Yii::$app->request->post())) {
            $model->alias=(new Translit())->str2url($model->name);
            if(UploadedFile::getInstance($model, 'cat_img'))
            {
                $model->cat_img=UploadedFile::getInstance($model, 'cat_img');
                $model->cat_img->saveAs('../../frontend/web/mt/img/'.$model->cat_img->baseName.".".$model->cat_img->extension);
                Image::thumbnail('../../frontend/web/mt/img/'.$model->cat_img->baseName.".".$model->cat_img->extension, 350, 365)->save(Yii::getAlias('../../frontend/web/mt/img/'.$model->cat_img->baseName.".".$model->cat_img->extension), ['quality' => 50]);
                $model->cat_img=$model->cat_img->baseName.".".$model->cat_img->extension;    
            }
            else
                $model->cat_img=$img;
            if(UploadedFile::getInstance($model, 'banner_img'))
            {
                $model->banner_img=UploadedFile::getInstance($model, 'banner_img');
                $model->banner_img->saveAs('../../frontend/web/mt/img/'.$model->banner_img->baseName.".".$model->banner_img->extension);
                Image::thumbnail('../../frontend/web/mt/img/'.$model->banner_img->baseName.".".$model->banner_img->extension, 1140, 350)->save(Yii::getAlias('../../frontend/web/mt/img/'.$model->banner_img->baseName.".".$model->banner_img->extension), ['quality' => 50]);
                $model->banner_img=$model->banner_img->baseName.".".$model->banner_img->extension;
            }
            else
                $model->banner_img=$img2;
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionDeletebanner($id)
    {
        $model = $this->findModel($id);
        $model->banner_img= '';
        $model->save();
        return $this->redirect(['update', 'id' => $model->id]);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
