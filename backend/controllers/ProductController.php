<?php

namespace backend\controllers;

use Yii;
use common\models\Product;
use common\models\ProductAttribute;
use common\models\ProductSearch;
use common\models\ThemeSettings;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\imagine\Image;
use common\components\Translit;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends MyController
{
    /**
     * @inheritdoc
     */
    /*public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }*/

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $product = new Product();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if ($product->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post()['Product'];
            
            /** change price */
            if (isset($post['price'])) {
                $object = $this->findModel($post['id']);
                $object->price = $post['price'];
                $object->save();
            }

            /** increase price */
            if (isset($post['increase_price']) && !empty($post['increase_price'])) {
                $object = $product->find()->all();

                foreach ($object as $number => $product) {
                    $product->price += $post['increase_price'];
                    $product->save();
                }
            }

            /** decrease_price */
            if (isset($post['decrease_price']) && !empty($post['decrease_price'])) {
                $object = $product->find()->all();

                foreach ($object as $number => $product) {

                    if ($product->price < $post['decrease_price'])
                        $product->price = 0;
                    else
                        $product->price -= $post['decrease_price'];

                    $product->save();
                }
            }
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'product' => $product,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();
        $modelTheme = ThemeSettings::findOne(['id'=>1]);
        $str="";

        if ($model->load(Yii::$app->request->post())) {
            $attributes = $model->attributes;

            if ($attributes) {
                foreach ($attributes as $number => $value) {
                    $attributes[$number] = explode(',', $value);
                }
            }

            $model->attributes = json_encode($attributes);
            //return $this->redirect(['view', 'id' => $model->id]);
            if(UploadedFile::getInstances($model, 'photos'))
            {
                $model->photos=UploadedFile::getInstances($model, 'photos');
                foreach ($model->photos as $file) {
                    $file->saveAs('../../frontend/web/mt/img/'.$file->baseName.".".$file->extension);
                    Image::thumbnail('../../frontend/web/mt/img/'.$file->baseName.".".$file->extension, 440, 420)->save(Yii::getAlias('../../frontend/web/mt/img/'.$file->baseName.".".$file->extension), ['quality' => 50]);
                    $str.=$file->baseName.".".$file->extension."##";
                }
            }
            if(UploadedFile::getInstance($model, 'main_img'))
            {
                $model->main_img=UploadedFile::getInstance($model, 'main_img');
                $model->main_img->saveAs('../../frontend/web/mt/img/'.$model->main_img->baseName.".".$model->main_img->extension);
                Image::thumbnail('../../frontend/web/mt/img/'.$model->main_img->baseName.".".$model->main_img->extension, 440, 420)->save(Yii::getAlias('../../frontend/web/mt/img/'.$model->main_img->baseName.".".$model->main_img->extension), ['quality' => 50]);
                $model->main_img=$model->main_img->baseName.".".$model->main_img->extension;
            }
            $model->photos=$str;
            $model->alias=(new Translit())->str2url($model->name);
            $model->sizes = '';// implode(", ", $model->sizes);
            //$model->alias=(new Product)->str2url($model->name);
            if ($model->save())
                return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model'         => $model,
                'model_theme'   => $modelTheme,
            ]);
        }
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if($model->photos)
        {
            $str=$model->photos;
        }
        else {
            $str='';
        }
        $img=$model->main_img;
        if ($model->load(Yii::$app->request->post())) {
            $model->sizes=implode(", ", $model->sizes);
            $attributes = $model->attributes;

            if ($attributes) {
                foreach ($attributes as $number => $value) {
                    $attributes[$number] = explode(',', $value);
                }
            }

            $model->attributes = json_encode($attributes);
            //return $this->redirect(['view', 'id' => $model->id]);
            if(UploadedFile::getInstances($model, 'photos'))
            {
                $model->photos=UploadedFile::getInstances($model, 'photos');
                if(!isset($str)) {$str="";}
                foreach ($model->photos as $file) {
                    $file->saveAs('../../frontend/web/mt/img/'.$file->baseName.".".$file->extension);
                    Image::thumbnail('../../frontend/web/mt/img/'.$file->baseName.".".$file->extension, 440, 420)->save(Yii::getAlias('../../frontend/web/mt/img/'.$file->baseName.".".$file->extension), ['quality' => 50]);
                    $str.=$file->baseName.".".$file->extension."##";
                }
            }
            if(UploadedFile::getInstance($model, 'main_img'))
            {
                $model->main_img=UploadedFile::getInstance($model, 'main_img');
                $model->main_img->saveAs('../../frontend/web/mt/img/'.$model->main_img->baseName.".".$model->main_img->extension);
                Image::thumbnail('../../frontend/web/mt/img/'.$model->main_img->baseName.".".$model->main_img->extension, 440, 420)->save(Yii::getAlias('../../frontend/web/mt/img/'.$model->main_img->baseName.".".$model->main_img->extension), ['quality' => 50]);
                $model->main_img=$model->main_img->baseName.".".$model->main_img->extension;
            }
            else
                $model->main_img=$img;
            $model->photos=$str;
            $model->alias=(new Translit())->str2url($model->name);
            //$model->alias=(new Product)->str2url($model->name);
            if ($model->save())
                return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    public function actionDeletephoto($id, $name)
    {
        $model = $this->findModel($id);
        $model->photos= str_replace ( $name."##", "", $model->photos);
        $model->save();
        return $this->redirect(['update', 'id' => $model->id]);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
