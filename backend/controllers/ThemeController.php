<?php

namespace backend\controllers;

use Yii;
use common\models\ThemeSettings;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ThemeController implements the CRUD actions for ThemeSettings model.
 */
class ThemeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ThemeSettings models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ThemeSettings::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ThemeSettings model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ThemeSettings model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ThemeSettings();

        if ($model->load(Yii::$app->request->post())) {
            if(UploadedFile::getInstance($model, 'main_photo'))
            {
                $model->main_photo=UploadedFile::getInstance($model, 'main_photo');
                $model->main_photo->saveAs('../../frontend/web/mt/img/'.$model->main_photo->baseName.".".$model->main_photo->extension);
                $model->main_photo=$model->main_photo->baseName.".".$model->main_photo->extension;
            }
            if ($model->save())
                return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ThemeSettings model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $img=$model->main_photo;
        if ($model->load(Yii::$app->request->post())) {
            if(UploadedFile::getInstance($model, 'main_photo'))
            {
                $model->main_photo=UploadedFile::getInstance($model, 'main_photo');
                $model->main_photo->saveAs('../../frontend/web/mt/img/'.$model->main_photo->baseName.".".$model->main_photo->extension);
                $model->main_photo=$model->main_photo->baseName.".".$model->main_photo->extension;
            }
            else
                $model->main_photo=$img;
           if ($model->save())
                return $this->redirect(['view', 'id' => 1]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ThemeSettings model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ThemeSettings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ThemeSettings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ThemeSettings::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
