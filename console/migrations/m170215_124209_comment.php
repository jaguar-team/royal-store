<?php

use yii\db\Migration;

class m170215_124209_comment extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%comment}}', [
            'id'            => $this->primaryKey(),
            'id_product'    => $this->integer(10)->notNull(),
            'name'          => $this->string(255),
            'comment'       => $this->text(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%comment}}');
    }
}
