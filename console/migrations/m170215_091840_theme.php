<?php

use yii\db\Migration;

class m170215_091840_theme extends Migration
{
    public $data = [
        'shop_name'             => 'StoreKids',
        'shop_url'              => 'store-kids.com.ua',
        'times'                 => 'Пн - пт, 09:00 - 17:00',
        'phone1'                => '+46 (0)8 663 50 60',
        'phone2'                => '+46 (0)8 663 50 60',
        'email'                 => 'support@store-kids.com.ua',
        'adress'                => 'Украина, Киев, пл. Конститукции 54',
        'vk'                    => 'httts://vk.com',
        'fb'                    => 'https://facebook.com',
        'tw'                    => 'https://twitter.com',
        'odn'                   => 'https://ok.ru',
        'main_photo'            => 'logo_header_v3.png',
        'main_title'            => 'Заголовок гланой страницы',
        'main_text'             => 'Добро пожаловать в интернет-магазин Royal Store! В нашем интернет-магазине Вы имеете возможность заказать оригинальные товары класса люкс. Доставка осуществляется в течение всего 3 рабочих дней! Если заказанный товар вам не подходит, мы без лишних вопросов поменяем его, или вернем деньги. Так, кто вам сказал, что брендовые вещи не купишь дёшево?! ​Мы работаем для Вас!',
    ];

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%theme_settings}}', [
            'id'                    => $this->primaryKey(),
            'shop_name'             => $this->string(255)->notNull(),
            'shop_url'              => $this->string(255)->notNull(),
            'times'                 => $this->text()->notNull(),
            'phone1'                => $this->text()->notNull(),
            'phone2'                => $this->text()->notNull(),
            'email'                 => $this->string(255)->notNull(),
            'adress'                => $this->text()->notNull(),
            'vk'                    => $this->string(255),
            'fb'                    => $this->string(255),
            'tw'                    => $this->string(255),
            'odn'                   => $this->string(255),
            'main_photo'            => $this->string(255)->notNull(),
            'main_title'            => $this->string(255)->notNull(),
            'main_text'             => $this->text()->notNull(),
            'about_page'            => $this->text()->notNull(),
            'delivery_page'         => $this->text()->notNull(),
            'contactus_page_title'  => $this->text()->notNull(),
            'contactus_page_text'   => $this->text()->notNull(),
            'contactus_page_map'    => $this->text()->notNull(),
            'seo_main_title'        => $this->text()->notNull(),
            'seo_main_keys'         => $this->text()->notNull(),
            'seo_main_desc'         => $this->text()->notNull(),
            'seo_about_title'       => $this->text()->notNull(),
            'seo_about_keys'        => $this->text()->notNull(),
            'seo_about_desc'        => $this->text()->notNull(),
            'seo_delivery_title'    => $this->text()->notNull(),
            'seo_delivery_keys'     => $this->text()->notNull(),
            'seo_delivery_desc'     => $this->text()->notNull(),
            'seo_contacts_title'    => $this->text()->notNull(),
            'seo_contacts_keys'     => $this->text()->notNull(),
            'seo_contacts_desc'     => $this->text()->notNull(),
            'return_page'           => $this->text()->notNull(),
            'seo_return_title'      => $this->text()->notNull(),
            'seo_return_desc'       => $this->text()->notNull(),
            'seo_return_keys'       => $this->text()->notNull(),
        ], $tableOptions);

        $this->insert('{{%theme_settings}}', $this->data);
    }

    public function down()
    {
        $this->dropTable('{{%theme_settings}}');
    }
}
