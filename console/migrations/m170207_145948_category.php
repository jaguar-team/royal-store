<?php

use yii\db\Migration;

class m170207_145948_category extends Migration
{
    public $data = [
        /** level 1 **/
        [
            'name'          => 'Одежда девочки',
            'desc'          => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'banner_img'    => 'default_category_img_1.jpg',
            'seo_title'     => 'Одежда девочки',
            'seo_desc'      => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'seo_keys'      => 'одежда, девочки',
        ],
        [
            'name'          => 'Одежда мальчики',
            'desc'          => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'banner_img'    => 'default_category_img_2.jpg',
            'seo_title'     => 'Одежда мальчики',
            'seo_desc'      => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'seo_keys'      => 'одежда, мальчики',
        ],
        [
            'name'          => 'Обувь девочки',
            'desc'          => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'banner_img'    => 'default_category_img_3.jpg',
            'seo_title'     => 'Обувь девочки',
            'seo_desc'      => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'seo_keys'      => 'обувь, девочки',
        ],
        [
            'name'          => 'Обувь мальчики',
            'desc'          => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'banner_img'    => 'default_category_img_4.jpg',
            'seo_title'     => 'Обувь мальчики',
            'seo_desc'      => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'seo_keys'      => 'обувь, мальчики',
        ],
        [
            'name'          => 'Аксессуары девочки',
            'desc'          => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'banner_img'    => 'default_category_img_5.jpg',
            'seo_title'     => 'Аксессуары девочки',
            'seo_desc'      => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'seo_keys'      => 'акссесуары, девочки',
        ],
        [
            'name'          => 'Акссесуары мальчики',
            'desc'          => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'banner_img'    => 'default_category_img_6.jpg',
            'seo_title'     => 'Акссесуары мальчики',
            'seo_desc'      => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'seo_keys'      => 'акссесуары, мальчики',
        ],
        /** level 2 **/
        /** level 3 **/
    ];

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%category}}', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string()->notNull()->unique(),
            'desc'          => $this->text(),
            'alias'         => $this->string(),
            'position'      => $this->integer(10),
            'parent'        => $this->integer()->defaultValue(0),
            'cat_img'       => $this->string(),
            'seo_title'     => $this->string(),
            'seo_keys'      => $this->string(),
            'seo_desc'      => $this->string(),
            'banner_img'    => $this->string(),
            'banner_link'   => $this->string(),
        ], $tableOptions);


        foreach ($this->data as $number => $data) {
            $this->insert('{{%category}}', $data);
        }
    }

    public function down()
    {
        $this->dropTable('{{%category}}');
    }
}
