<?php

use yii\db\Migration;

class m170209_104420_product extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%product}}', [
            'id'                    => $this->primaryKey(),
            'name'                  => $this->string(255)->notNull(),
            'position'              => $this->integer(10),
            'code'                  => $this->string(255)->notNull(),
            'amount'                => $this->integer(10)->notNull(),
            'top'                   => $this->smallInteger(1)->defaultValue(0),
            'featured'              => $this->smallInteger(1)->defaultValue(0),
            'news'                  => $this->smallInteger(1)->defaultValue(0),
            'sizes'                 => $this->string(255)->notNull(),
            'made'                  => $this->string(255),
            'price'                 => $this->double()->notNull(),
            'discount'              => $this->integer(10),
            'attributes'            => $this->text(),
            'main_img'              => $this->string(255),
            'photos'                => $this->text(),
            'desc'                  => $this->text(),
            'about_brand'           => $this->text(),
            'category_id'           => $this->integer(10),
            'seo_title'             => $this->string(255),
            'seo_keys'              => $this->text(),
            'seo_desc'              => $this->text(),
            'alias'                 => $this->string(255)
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%product}}');
    }
}
