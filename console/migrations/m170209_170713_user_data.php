<?php

use yii\db\Migration;

class m170209_170713_user_data extends Migration
{
    public function up()
    {
        $this->insert('{{%user}}', [
            'username'              => 'admin',
            'auth_key'              => 'jSg0vceyGqnSZ_6WK7cTROvf3irdddCs',
            'password_hash'         => '$2y$13$OOwTky8ZC0VLsWAnrsb1.eXHfjimHhlz6Ydw7h6yVmWIGyRddNGwe',
            'password_reset_token'  => 'IGmdrKQaWNpzSe4PrldS_SEdL4CVXKRC_1480078675',
            'email'                 => 'admin@admin.ru',
            'status'                => '10',
            'created_at'            => '1471167953',
            'updated_at'            => '1480078675',
        ]);
    }

    public function down()
    {
        $this->delete('{{%user}}', ['id' => 1]);
    }
}
