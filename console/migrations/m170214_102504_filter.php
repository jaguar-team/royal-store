<?php

use yii\db\Migration;

class m170214_102504_filter extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%filter}}', [
            'id'            => $this->primaryKey(),
            'type'          => $this->string(),
            'value'         => $this->string(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%filter}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
