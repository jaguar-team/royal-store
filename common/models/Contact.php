<?php

namespace common\models;
use yii\helpers\Html;
use Yii;

/**
 * This is the model class for table "contact".
 *
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property string $text
 * @property string $date
 */
class Contact extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'text', 'date'], 'required'],
            [['text'], 'string'],
            [['name', 'phone', 'date'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'text' => 'Сообщение',
            'date' => 'Дата',
        ];
    }
    public function saveContact()
    {
        $this->name=Html::encode($this->name);
        $this->phone=Html::encode($this->phone);
        $this->text=Html::encode($this->text);
        $this->date=date('U');
        if($this->validate())
            $this->save();
        return true;
    }
}
