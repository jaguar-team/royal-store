<?php
/**
 * Created by PhpStorm.
 * User: vlad.dneprov
 * Date: 08.02.2017
 * Time: 11:46
 */

namespace common\models;

use common\models\ProductAttributeValue;

class ProductAttribute extends \yii\db\ActiveRecord
{
    public $quantity;

    public $productAttributeValueModel;

    public function __construct()
    {
        $this->productAttributeValueModel = new ProductAttributeValue();
        parent::__construct();
    }

    public static function tableName()
    {
        return 'product_attribute';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'name'      => 'Имя атрибута',
            'value'     => 'Значение атрибута',
            'quantity'  => 'Количестов значений',
        ];
    }
    
    public function getAll($null = true)
    {
        $attributes = $this->find()->all();
        if ($null) {
            return $attributes;
        } else {
            foreach ($attributes as $number => $attribute) {
                if ($attribute->isEmptyAttribute()) {
                    unset($attributes[$number]);
                }
            }

            return $attributes;
        }

    }

    public function isEmptyAttribute()
    {
        $productAttributeModel = new ProductAttributeValue();

        if ($productAttributeModel->getQuantityValuesByIdAttribute($this->id) > 0) {
            return false;
        }

        return true;
    }

    public function getQuantityValue()
    {
        $object = ProductAttributeValue::find()->where(['id_attribute' => $this->id])->all();

        return count($object);
    }
}