<?php

namespace common\models;
use yii\web\UploadedFile;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property string $cat_img
 * @property string $seo_title
 * @property string $seo_keys
 * @property string $seo_desc
 *
 * @property Product[] $products
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'alias'], 'required'],
            [['position', 'parent'], 'integer'],
            [['seo_keys', 'seo_desc', 'banner_link', 'desc'], 'string'],
            ['cat_img','file','skipOnEmpty' => true, 'extensions' => 'png, jpg', 'checkExtensionByMimeType'=>false],
            [['name', 'alias', 'seo_title', 'banner_img', 'banner_link'], 'string', 'max' => 255],
            ['banner_img','file','skipOnEmpty' => true, 'extensions' => 'png, jpg', 'checkExtensionByMimeType'=>false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'desc' => 'Описание',
            'parent' => 'Родительская котигория',
            'alias' => 'Адрес',
            'position'=>'Позиция',
            'cat_img' => 'Иконка категории',
            'seo_title' => 'Seo Заголовок',
            'seo_keys' => 'Seo Ключевые слова',
            'seo_desc' => 'Seo Описание',
            'banner_img' => 'Баннер картинка',
            'banner_link' => 'Баннер ссылка',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['category_id' => 'id']);
    }
    
    public function getAll()
    {
        return static::find()->orderBy('position')->all();
    }

    public function getHome()
    {
        return static::find()->orderBy('position')->limit(6)->all();
    }

    public function getChildren()
    {
        return $this->find()->where(['parent' => $this->id])->all();
    }

    public function getCategoryNameById($id)
    {
        $object = $this->find()->select(['name'])->where(['id' => $id])->one();
        return $object->name;
    }
}
