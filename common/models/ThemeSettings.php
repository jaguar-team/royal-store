<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "theme_settings".
 *
 * @property integer $id
 * @property string $times
 * @property string $email
 * @property string $adress
 * @property string $vk
 * @property string $fb
 * @property string $tw
 * @property string $odn
 * @property string $main_photo
 * @property string $main_title
 * @property string $main_text
 * @property string $about_page
 * @property string $delivery_page
 * @property string $contactus_page_title
 * @property string $contactus_page_text
 * @property string $contactus_page_map
 * @property string $seo_main_title
 * @property string $seo_main_keys
 * @property string $seo_main_desc
 * @property string $seo_about_title
 * @property string $seo_about_keys
 * @property string $seo_about_desc
 * @property string $seo_delivery_title
 * @property string $seo_delivery_keys
 * @property string $seo_delivery_desc
 * @property string $seo_contacts_title
 * @property string $seo_contacts_keys
 * @property string $seo_contacts_desc
 */
class ThemeSettings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'theme_settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['times', 'email', 'adress', 'main_title', 'main_text', 'shop_name', 'shop_url', 'phone1', 'phone2'], 'required'],
            [['times', 'adress', 'main_text', 'about_page', 'delivery_page', 'contactus_page_text', 'contactus_page_map', 'seo_main_keys', 'seo_main_desc', 'seo_about_keys', 'seo_about_desc', 'seo_delivery_keys', 'seo_delivery_desc', 'seo_contacts_keys', 'seo_contacts_desc', 'return_page', 'seo_return_title', 'seo_return_desc', 'seo_return_keys'], 'string'],
            [['email', 'vk', 'fb', 'tw', 'odn', 'main_title', 'contactus_page_title', 'seo_main_title', 'seo_about_title', 'seo_delivery_title', 'seo_contacts_title'], 'string', 'max' => 255],
            ['main_photo','file','skipOnEmpty' => true, 'extensions' => 'png, jpg', 'checkExtensionByMimeType'=>false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'shop_name' => 'Название магазина',
            'shop_url'  => 'Url-адрес магазина',
            'times' => 'Время работы',
            'phone1' => 'Телефон (1)',
            'phone2' => 'Телефон (2)',
            'email' => 'Email',
            'adress' => 'Адрес',
            'vk' => 'Группа VK',
            'fb' => 'Группа Fb',
            'tw' => 'Twitter',
            'odn' => 'Одноклассники',
            'main_photo' => 'Логотип сайта',
            'main_title' => 'Заголовок на главной',
            'main_text' => 'Текст на главной',
            'about_page' => 'Страничка "О нас"',
            'delivery_page' => 'Страничка "Оплата и доставка"',
            'contactus_page_title' => 'Заголовок страницы "Контакты"',
            'contactus_page_text' => 'Текст страницы "Контакты"',
            'contactus_page_map' => 'Карта страницы "Контакты"',
            'seo_main_title' => 'Главная: Seo Заголовок ',
            'seo_main_keys' => 'Главная: Seo Ключевые слова',
            'seo_main_desc' => 'Главная: Seo Описание',
            'seo_about_title' => 'О нас: Seo Заголовок',
            'seo_about_keys' => 'О нас: Seo Ключевые слова',
            'seo_about_desc' => 'О нас: Seo Описание',
            'seo_delivery_title' => 'Оплата и доставка: Seo Заголовок',
            'seo_delivery_keys' => 'Оплата и доставка: Seo Ключевые слова',
            'seo_delivery_desc' => 'Оплата и доставка: Seo Описание',
            'seo_contacts_title' => 'Контакты: Seo Заголовок',
            'seo_contacts_keys' => 'Контакты: Seo Ключевые слова',
            'seo_contacts_desc' => 'Контакты: Seo Описание',
            'return_page' => 'Страничка "Возврат и обмен"',
            'seo_return_title' => 'Возврат и обмен: Seo Заголовок',
            'seo_return_desc' => 'Возврат и обмен: Seo Описание',
            'seo_return_keys' => 'Возврат и обмен: Seo Ключевые слова',
        ];
    }
}
