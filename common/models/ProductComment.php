<?php
/**
 * Created by PhpStorm.
 * User: vlad.dneprov
 * Date: 15.02.2017
 * Time: 14:46
 */

namespace common\models;
use common\models\Product;

class ProductComment extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'comment';
    }

    public function rules()
    {
        return [
            [['id_product','name', 'comment'], 'required'],
            [['name', 'comment'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'id_product'        => 'Название товара',
            'name'              => 'Имя',
            'comment'           => 'Комментарий',
        ];
    }

    public function getCommentsByProductId($id_product)
    {
        return $this->find()->where(['id_product' => $id_product])->all();
    }

    public function getProductById()
    {
        return Product::find()->where(['id' => $this->id_product])->one();
    } 
}