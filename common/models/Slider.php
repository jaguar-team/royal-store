<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Slider".
 *
 * @property integer $id
 * @property string $name
 */
class Slider extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['img'], 'file'],
            [['alt'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alt' => 'Заголовок',
            'img' => 'Баннер',
        ];
    }

    public function getSlides($numbers = 4)
    {
        return $this->find()->all();
    }
}
