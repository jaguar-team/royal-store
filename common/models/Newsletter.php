<?php

namespace common\models;

class Newsletter extends \yii\db\ActiveRecord
{
    public $email;

    public static function tableName()
    {
        return false;
    }

    public function rules()
    {
        return [
            [['email'], 'email'],
            [['email'], 'required']
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
        ];
    }
}