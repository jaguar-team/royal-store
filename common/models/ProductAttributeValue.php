<?php
/**
 * Created by PhpStorm.
 * User: vlad.dneprov
 * Date: 08.02.2017
 * Time: 11:46
 */

namespace common\models;

class ProductAttributeValue extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'product_attribute_value';
    }

    public function rules()
    {
        return [
            [['value', 'id_attribute'], 'required'],
            [['value'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'id_attribute'  => 'Атрибут',
            'value'         => 'Значение атрибута',
        ];
    }

    public function getValuesByIdAttribute($id)
    {
        return $this->find()->where(['id_attribute' => $id])->all();
    }

    public function getQuantityValuesByIdAttribute($id)
    {
        return $this->find()->where(['id_attribute' => $id])->count();
    }
}