<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property string $city
 * @property string $office
 * @property string $prod_ids
 * @property string $prod_counts
 * @property string $prod_sizes
 * @property double $total
 * @property string $status
 * @property string $archive
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'city', 'office', 'prod_ids', 'prod_counts', 'prod_sizes', 'total'], 'required'],
            [['office', 'prod_ids', 'prod_counts', 'prod_sizes'], 'string'],
            [['total'], 'number'],
            ['archive', 'integer'],
            [['name', 'phone', 'city', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'phone' => 'Phone',
            'city' => 'City',
            'office' => 'Office',
            'prod_ids' => 'Prod Ids',
            'prod_counts' => 'Prod Counts',
            'prod_sizes' => 'Prod Sizes',
            'total' => 'Total',
            'status' => 'Status',
            'archive' => 'Archive',
        ];
    }
}
