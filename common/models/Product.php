<?php

namespace common\models;
use yii\data\Pagination;
use common\models\ProductAttribute;
use common\models\ProductAttributeValue;
use yii\web\UploadedFile;
use Yii;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property integer $amount
 * @property double $price
 * @property integer $discount
 * @property string $main_img
 * @property string $photos
 * @property string $desc
 * @property string $about_brand
 * @property integer $category_id
 * @property string $seo_title
 * @property string $seo_keys
 * @property string $sei_desc
 * @property string $alias
 *
 * @property Category $category
 */
class Product extends \yii\db\ActiveRecord
{
    public $increase_price;

    public $decrease_price;
    /**
     * @inheritdoc
     */

    public $productAttributeModel;
    public $productAttributeValueModel;

    public function __construct()
    {
        $this->productAttributeModel = new ProductAttribute();
        $this->productAttributeValueModel = new ProductAttributeValue();

        parent::__construct();
    }

    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code', 'amount', 'price', 'category_id', 'attributes'], 'required'],
            [['amount', 'discount', 'category_id', 'top', 'featured', 'news', 'position'], 'integer'],
            [['price'], 'number'],
            [['desc', 'about_brand', 'seo_keys', 'seo_desc'], 'string'],
            [['name', 'code', 'seo_title', 'alias', 'made'], 'string', 'max' => 255],
            ['sizes', 'safe'],
            ['main_img','file','skipOnEmpty' => true, 'extensions' => 'png, jpg', 'checkExtensionByMimeType'=>false],
            [['photos'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'maxFiles' => 20, 'checkExtensionByMimeType'=>false],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
    public function getAllAttributes()
    {
        $productAttributeModel = new ProductAttribute();
        $productAttributeValueModel = new ProductAttributeValue();

        $attributes = $productAttributeModel->getAll();

        $return = [];
        foreach ($attributes as $number => $attribute) {
            if (!$attribute->isEmptyAttribute($attribute->id)) {
                $return[] = $attribute;
            }
        }

        var_dump($return);

        return $return;
    } **/
    
    public $r_price;
    public function afterFind()
    {
        if($this->discount)
            $this->r_price=(1-$this->discount/100)*$this->price;
        else
            $this->r_price=$this->price;
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'positon'=>'Позиция',
            'code' => 'Код товара',
            'amount' => 'Наличие',
            'position'=>'Позиция',
            'sizes'=>'Размеры',
            'price' => 'Цена (в гривнах)',
            'discount' => 'Скидка (в процентах)',
            'main_img' => 'Главное фото',
            'photos' => 'Фотографии',
            'desc' => 'Описание товара',
            'about_brand' => 'О бренде',
            'category_id' => 'Категория',
            'seo_title' => 'Seo Заголовок',
            'seo_keys' => 'Seo Ключевые слова',
            'seo_desc' => 'Seo Описание',
            'alias' => 'name-url',
            'top' => 'Топ товар',
            'news' => 'Новинки',
            'made' => 'Производитель',
            'attributes' => 'Атрибуты',

            'increase_price' => 'Увеличить цену на все товары',
            'decrease_price' => 'Понизить цену на все товары',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
    public function getAllinAlias($id)
    {
        if ($id) {
            $query = static::find()->where(['category_id'=>$id])->distinct();
        } else {
            $query =static::find();
        }

        /** Pagination **/
        $pagination = new Pagination([
            'totalCount'        =>$query->count(),
            'defaultPageSize'   => 8
        ]);
            
        $query = $query->orderBy('position');
        $products = $query->offset($pagination->offset)->limit($pagination->limit)->all();

        return ['products'=>$products, 'pagination'=>$pagination];
    }
    
    public function getProd($alias)
    {
        $product = static::find()->where(['alias' => $alias])->one();//мы находимся и так в этом классе, грамотнее писать статик либо селф
        return $product;
    }
    
    public function getAll()
    {
        return static::find()->orderBy('position')->all();
    }
    
    public function findProductsByIds($ids)
    {
        $products=array();
        for($i=0;$i<count($ids);$i++)
            $products[]=self::find()->where(['id'=>$ids[$i]['id']])->one();
        return $products;
    }
    public function findProductsByOrderIds($ids)
    {
        $products=array();
        for($i=0;$i<count($ids);$i++)
            $products[]=static::find()->where(['id'=>$ids[$i]])->one();
        return $products;
    }
    public function getAllinSearch($q)
    {
        $query = static::find()
            ->leftJoin('category', '`category`.`id`=`product`.`category_id`')
            ->where(['like', 'product.name', $q])
            ->orWhere(['like', 'product.desc', $q])
            ->orWhere(['like', 'product.code', $q])
            ->orWhere(['like', 'product.alias', $q])
            ->orWhere(['like', 'category.name', $q])
            ->orWhere(['like', 'category.alias', $q])
            ->with('category')
            ->distinct();
        $pagination= new Pagination([
            'totalCount'=>$query->count(),//общее количество товаров
            'defaultPageSize'=>8//количесво товаров на одной странице
        ]);
        $products=$query->offset($pagination->offset)//смещение = текущая страница*defaultPageSize
            ->limit($pagination->limit)//лимит записей =defaultPageSize
            ->all();
        
        return ['products'=>$products, 'pagination'=>$pagination];
    }

    public function getFilterProduct($query)
    {
        $type = $query['type'];
        $id_category = $query['id'];
        switch ($type) {
            case 'attr':
                $products = $this->find()->where(['category_id' => $id_category])->all();

                $attr_id = $query['attr_id'];
                $attr_value_id = $query['attr_value_id'];
                foreach ($products as $number => $product) {
                    $attrs = json_decode($product->attributes);
                    $unset = true;
                    foreach ($attrs as $attr_number => $attr) {
                        if ($attr[0] == $attr_id && $attr[1] == $attr_value_id) {
                            $unset = false;
                            break;
                        }
                    }
                    if ($unset) {
                        unset($products[$number]);
                    }
                }
                break;
            case 'price':
                $products = $this->find()->where(['category_id' => $id_category])->orderBy('price DESC')->all();
                break;
            case 'category':
                $id_sub_category = $query['id_sub_category'];
                $products = $this->find()->where(['category_id' => $id_sub_category])->all();
                break;
            default:
                $products = $this->find()->where(['category_id' => $id_category])->all();
                break;
        }

        $pagination= new Pagination([
            'totalCount'=>count($products),//общее количество товаров
            'defaultPageSize'=>8//количесво товаров на одной странице
        ]);

        return ['products'=>$products, 'pagination'=>$pagination];
    }
    
    public function getTop()
    {
        return static::find()
        ->where(['top' => 1])
        ->orderBy('position')
        ->all();
    }

    public function getFeatured()
    {
        return static::find()
        ->where(['featured' => 1])
        ->orderBy('position')
        ->all();
    }

    public function getNews()
    {
        return static::find()
        ->where(['news' => 1])
        ->orderBy('position')
        ->all();
    }
}
