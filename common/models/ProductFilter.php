<?php

namespace common\models;

use Yii;

class ProductFilter extends \yii\db\ActiveRecord
{
    public $f_general;

    public $f_attr;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'filter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'value'], 'required'],
            [['f_general', 'f_attr'], function ($attribute, $params) {
                if(!is_array($params)){
                    $this->addError('x','X is not array!');
                }
            }]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'type'          => 'Тип',
            'value'         => 'Значение',
            'f_general'     => 'Основные фильтры',
            'f_attr'        => 'Фильтры аттрибутов',
        ];
    }
}