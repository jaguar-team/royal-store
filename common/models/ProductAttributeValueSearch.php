<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ProductAttributeValue;

class ProductAttributeValueSearch extends ProductAttributeValue
{

    public function rules()
    {
        // only fields in rules() are searchable
        return [
            [['id'], 'integer'],
            [['value'], 'string'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = ProductAttributeValue::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (isset($params['id']))
            $query->andFilterWhere(['id_attribute' => $params['id']]);

        // load the search form data and validate
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // adjust the query by adding the filters
        $query->andFilterWhere(['id' => 1]);
        $query->andFilterWhere(['value' => $this->value]);

        return $dataProvider;
    }
}