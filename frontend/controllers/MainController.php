<?php

namespace frontend\controllers;
use common\models\Product;
use common\models\ProductComment;
use common\models\ProductAttribute;
use common\models\Category;
use common\models\Slider;
use frontend\models\Search;
use common\models\ThemeSettings;
use Yii;
use yii\helpers\Html;
use yii\helpers\Json;

class MainController extends \yii\web\Controller
{
    public $layout = "mystock";

    public $param = 'value';

    /**
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        /** models */
        $slider     = \Yii::createObject(Slider::className());
        $product    = \Yii::createObject(Product::className());
        $search     = \Yii::createObject(Search::className());
        $category   = \Yii::createObject(Category::className());

        /** init variables */
        $slider = $slider->getSlides();
        $slider_top_products = $product->getTop();
        $categories = $category->getHome();

        if ($search->load(Yii::$app->request->post())) {
            return $this->redirect(['search', 'q'=>Html::encode($search->q)]);
        }

        return $this->render('index',[
            'slider'        => $slider,
            'categories'    => $categories,
            'top'           => $slider_top_products,
            'theme'         => ThemeSettings::findOne(['id'=>1]),
        ]);
    }
    
    public function actionProduct($alias)
    {
        $modelComment = new ProductComment();
        if ($modelComment->load(Yii::$app->request->post())) {
            if ($modelComment->save()) {
                $modelComment = new ProductComment();
                $this->refresh();
            }
        }
        $model = (new Product)->getProd($alias);
        $slider1 = (new Product)->getTop();
        $slider2 = (new Product)->getFeatured();
        $slider3 = (new Product)->getNews();return $this->render('product', [
            'comment'   => $modelComment,
            'prod'      => $model,
            'top'       => $slider1,
            'featured'  => $slider2,
            'news'      => $slider3,
        ]);
    }
    
    public function actionCatalog($id)
    {
        $query = Yii::$app->getRequest()->get();

        $productAttributeModel = new ProductAttribute();
        $productModel = new Product();

        /** filter */
        if (isset($query['type'])) {
            $model = $productModel->getFilterProduct($query);
        } else {
            $model = $productModel->getAllinAlias($id);
        }

        $cat = (new Category)->getAll();
        $category=Category::findOne(['id'=>$id]);
        return $this->render('catalog', [
            'products' => $model['products'],
            'attribute' => $productAttributeModel,
            'cat' => $cat,
            'pagination'=>$model['pagination'],
            'category'=>$category
        ]);
    }
    public function actionSearch($q)
    {
        if($q) $products=(new Product())->getAllinSearch($q);
        return $this->render('search',[
            'products' => $products['products'],
            'q'=>$q,
            'pagination'=>$products['pagination']
        ]);
    }
    public function actionAddtowishlist($id)
    {
        $ids=Yii::$app->request->cookies->getValue('wishlist');
        $ids[]=$id;
        Yii::$app->response->cookies->add(new \yii\web\Cookie([
            'name' => 'wishlist',
            'value' =>array_unique($ids),
        ]));
        
        echo Json::encode(['flag'=>'1']);
    }
    public function actionDelfromwishlist($id)
    {
        $ids=Yii::$app->request->cookies->getValue('wishlist');
        $new_arr=[];
        foreach($ids as $oid)
        {
            if($oid!=$id)
            {
               $new_arr[]=$oid;
            }
        }
        Yii::$app->response->cookies->add(new \yii\web\Cookie([
            'name' => 'wishlist',
            'value' =>$new_arr
        ]));
        
        echo Json::encode(['flag'=>'1']);
    }
    /*public function actionTest()
    {
        //echo Yii::$app->getSecurity()->generatePasswordHash('ilovelife888');
        echo Yii::$app->mailer
                ->compose()
                ->setFrom([Yii::$app->params['supportEmail']=>'MY STOCK'])
                ->setTo('siegigor@gmail.com')
                ->setSubject('Новый заказ!')
                ->setHtmlBody("<h1>Новый заказ в интернет-магазине MY STOCK!</h1>")
                ->send();
    }*/
    public function actionDelivery()
    {
        return $this->render('delivery',[
            'theme'=>ThemeSettings::findOne(['id'=>1])
        ]);
    }
    public function actionAbout()
    {
        return $this->render('about',[
            'theme'=>ThemeSettings::findOne(['id'=>1])
        ]);
    }
    public function actionWishlist()
    {
        $ids=Yii::$app->request->cookies->getValue('wishlist');
        $products=(new Product())->findProductsByOrderIds($ids);
        return $this->render('wishlist', [
            'products'=>$products,
            'theme'         => ThemeSettings::findOne(['id'=>1]),
        ]);
    }
    
    public function actionContacts()
    {
        return $this->render('contacts',[
            'theme'=>ThemeSettings::findOne(['id'=>1])
        ]);   
    }
    
    public function actionReturn()
    {
        return $this->render('return',[
            'theme'=>ThemeSettings::findOne(['id'=>1])
        ]);   
    }
}
