<?php

namespace frontend\controllers;
use frontend\models\OrderForm;
use yii\helpers\Json;
use frontend\components\Cart;
use Yii;
use common\models\Product;
use frontend\components\NovaPoshta;
use common\models\ThemeSettings;

class CartController extends \yii\web\Controller
{
    public $layout="mystock";
    public function actionIndex()
    {
        $ids=Yii::$app->session->get('ids');
        $products=(new Product)->findProductsByIds($ids);
        return $this->render('index',[
            'products'=>$products,
            'ids'=>$ids,
            'theme'         => ThemeSettings::findOne(['id'=>1]),
        ]);
    }
    public function actionOrder()
    {
        if(!Yii::$app->session->has('ids'))
            $this->redirect(['index']);
        $model=new OrderForm();
        $thanks = 0;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) 
        {
            if($model->saveOrder())
                $thanks=1;
            return $this->render('order',[
                'model'=>$model,
                'thanks'=>$thanks,
                'theme'         => ThemeSettings::findOne(['id'=>1]),
            ]);
        }
        return $this->render('order',[
            'model'=>$model,
            'cityes'=>(new NovaPoshta())->getCityes(),
            'thanks'=>$thanks,
            'theme'         => ThemeSettings::findOne(['id'=>1]),
        ]);
    }
    public function actionGetoffice($city)
    {
      echo Json::encode((new NovaPoshta())->getOffices($city));
    }
    public function actionAddtocart($id, $amount=1, $size)
    {
        $cart = new Cart();
        if($amount>=1 && $size)
        {
            $cart->addToCart($id, $amount, $size);
            //return $this->redirect(Yii::$app->request->referrer);
            return $this->redirect(['index']);
            /*echo Json::encode(['price'=>Yii::$app->session->get('price'),
            'count'=>Yii::$app->session->get('count'),
            ]);*/
        }
        else
            return $this->redirect(Yii::$app->request->referrer);
    }
    public function actionPluscart($id,$size,$i)
    {
        $cart = new Cart();
        $cart->addToCart($id, 1, $size);
        echo Json::encode(['price'=>Yii::$app->session->get('price'),
            'count'=>Yii::$app->session->get('count'),
            'onecount'=>Yii::$app->session->get('ids')[$i]['count'],
            'oneprice'=>Product::find()->where(['id'=>$id])->one()->r_price
            ]);
    }
     public function actionMinuscart($id,$size,$i)
    {
        $cart = new Cart();
        if(Yii::$app->session->get('ids')[$i]['count']>=2)
            $cart->minusCart($id, $size);
        echo Json::encode(['price'=>Yii::$app->session->get('price'),
            'count'=>Yii::$app->session->get('count'),
            'onecount'=>Yii::$app->session->get('ids')[$i]['count'],
            'oneprice'=>Product::find()->where(['id'=>$id])->one()->r_price
            ]);
    }


    public function actionDelcart($id, $size)
    {
        //if(Yii::$app->session->has('pay_id'))
            //Yii::$app->session->remove('pay_id');
        $cart = new Cart();
        $cart->delFromCart($id, $size);
        return $this->redirect(Yii::$app->request->referrer);
    }
    public function actionUnset()
    {
        Yii::$app->session->remove('ids');
        Yii::$app->session->remove('count');
        Yii::$app->session->remove('price');
        return $this->redirect(['main/index']);
    }
}
