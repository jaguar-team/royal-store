<?php
namespace frontend\models;
use common\models\Order;
use yii\base\Model;
use yii\helpers\Html;
use yii\helpers\Url;
use Yii;
class OrderForm extends Model
{
    public $name;
    public $phone;
    public $city;
    public $ofice;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'phone', 'city', 'ofice'], 'required'],
            [['name', 'phone', 'city', 'ofice'], 'string']
        ];
    }
    public function attributeLabels()
    {
        return [
            'name' => 'ФИО',
            'phone' => 'Телефон',
            'city' => 'Город',
            'ofice' => '№ склада',
        ];
    }
    public function saveOrder()
    {
        $order=new Order();
        $order->name=Html::encode($this->name);
        $order->phone=Html::encode($this->phone);
        $order->city=Html::encode($this->city);
        $order->office=Html::encode($this->ofice);

        $str_ids = '';
        $str_count = '';
        $str_size = '';
        foreach(Yii::$app->session->get('ids') as $ids)
        {
            $str_ids.=$ids['id'].'##';
            $str_count.=$ids['count'].'##';
            $str_size.=$ids['size'].'##';
        }
        $order->prod_ids=substr($str_ids, 0, -2);
        $order->prod_counts=substr($str_count, 0, -2);
        $order->prod_sizes=substr($str_size, 0, -2);
        $order->total=Yii::$app->session->get('price');
        if ($order->save())
        {
            Yii::$app->mailer
                ->compose()
                ->setFrom([Yii::$app->params['supportEmail']=>'MY STOCK'])
                ->setTo('englandstock@bigmir.net')
                ->setSubject('Новый заказ!')
                ->setHtmlBody("<h1>Новый заказ в интернет-магазине MY STOCK!</h1>
                <ul>
                    <li><b>Имя: </b>$order->name</li>
                    <li><b>Телефон: </b>$order->phone</li>
                    <li><b>Город: </b>$order->city</li>
                    <li><b>Новая почта: </b>$order->office</li>
                    <li><b>На: </b>$order->total грн.</li>
                    <li><b>Перейти к деталям: </b><a href='http://my-stock.com.ua/yii-admin'>http://my-stock.com.ua/yii-admin</a></li>
                </ul>
                ")
                ->send();
            return true;
        }
        else 
            return false;
    }
}