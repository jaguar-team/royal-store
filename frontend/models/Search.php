<?php
    namespace frontend\models;
    
    use Yii;
    use yii\base\Model;
    
    class Search extends Model
    {
        public $q;
        
        public function rules()
        {
            return [
                //['q', 'required'],
                ['q', 'string']
            ];
        }
        public function attributeLabels()
        {
            return [
                'q' => '��������� ������',
            ];
        }
    }

?>