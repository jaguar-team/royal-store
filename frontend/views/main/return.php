<?php
/* @var $this yii\web\View */
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
$this->title=$theme->seo_return_title;

$this->registerMetaTag([ 
    'name'=>'description', 
    'content'=>$theme->seo_return_desc
]); 
$this->registerMetaTag([ 
    'name'=>'keywords', 
    'content'=>$theme->seo_return_keys
]);
$this->params['breadcrumbs'][] = 'Возврат и обмен';
?>

<div class="container delivery">
        
    <div class="row">
        <div class="col-sm-12">
            <?=$theme->return_page;?>
        </div>
    </div>
</div>