<?php 
use yii\helpers\Html;
use yii\helpers\Url;

$count=count($products);
$this->title = 'Избранное '.$theme->shop_name;
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="container">

<div class="wish_strip">
    <h1 class="wish_h1">Избранное</h1>
</div>
<?php if($count){?>
<h4 class="wish_h4">Количество продуктов: <span><?=count($products);?></span></h4>

    <div class="row wish_block">
    <?php foreach($products as $prod){?>
        <div class="col-md-6 col-sm-6 col-xs-12 prod<?=$prod->id;?>">
            <div class="wish_prod">
                <div class="row">
                    <div class="col-md-5 col-sm-5 col-xs-5 wish_left">
                        <span class="discount">- 15%</span>
                        <a href="<?=Url::toRoute(['main/product', 'alias' => $prod->alias]);?>">
                            <img src="/frontend/web/mt/img/<?=$prod->main_img;?>" alt="<?=$prod->id;?>" />
                        </a>
                    </div>
                    <div class="col-md-7 col-sm-7 col-xs-7 wish_right">
                    <span onclick="delfromwishlist(<?=$prod->id;?>)" class="close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        <h3><a href="<?=Url::toRoute(['main/product', 'alias' => $prod->alias]);?>"><?= $prod->name;?></a></h3>
                        <div class="wish_code">Код товара: <?= $prod->code;?></div>
                        
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <?php if($prod->discount) {?>
                                    <div class="wish_old_price"><?= $prod->price;?> грн.</div>
                                    <div class="wish_new_price">Цена: <?= $prod->r_price;?> грн.</div>
                                <?php } else { ?>
                                    <div style="margin-top: 32px;" class="wish_new_price">Цена: <?= $prod->r_price;?> грн.</div>
                                <?php } ?>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6 wb_block">
                                <a class="wish_button" href="<?=Url::toRoute(['main/product', 'alias' => $prod->alias]);?>"><i class="fa fa-shopping-bag" aria-hidden="true"></i> КУПИТЬ</a>
                            </div>
                        </div>   
                    </div>
                </div>        
            </div>
        </div>
    <?php } ?>
        
        
    </div>
    <?php } else {?>
    <div class="row">
        <div class="col-sm-12">
        <div class="alert alert-warning" role="alert">Вы еще не добавляли товары в избранное</div>
        </div>
    </div>
    <?php }?>
</div>