<?php
/* @var $this yii\web\View */
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
$this->title=$theme->seo_delivery_title;

$this->registerMetaTag([ 
    'name'=>'description', 
    'content'=>$theme->seo_delivery_desc
]); 
$this->registerMetaTag([ 
    'name'=>'keywords', 
    'content'=>$theme->seo_delivery_keys
]);
$this->params['breadcrumbs'][] = 'Оплата и доставка';
?>
<div class="container delivery">
    <div class="row">
        <div class="col-sm-12">
            <?=$theme->delivery_page;?>
        </div>
    </div>
</div>