<?php
/* @var $this yii\web\View */
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
$this->title=$theme->seo_about_title;

$this->registerMetaTag([ 
    'name'=>'description', 
    'content'=>$theme->seo_about_desc
]); 
$this->registerMetaTag([ 
    'name'=>'keywords', 
    'content'=>$theme->seo_about_keys
]);
$this->params['breadcrumbs'][] = 'О нас';
?>

<div class="container aboutus">
    <div class="row">
        <div class="col-sm-12">
            <?=$theme->about_page;?>
        </div>
    </div>
</div>