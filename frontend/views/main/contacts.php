<?php
use yii\helpers\Url;
$this->title=$theme->seo_contacts_title;
$this->registerMetaTag([ 
    'name'=>'description', 
    'content'=>$theme->seo_contacts_desc
]); 
$this->registerMetaTag([ 
    'name'=>'keywords', 
    'content'=>$theme->seo_contacts_keys
]);
$this->params['breadcrumbs'][] = 'Контакты';
?>

<div class="container contacts_page">
    <p>&nbsp;</p>

    <p style="text-align:center"><span style="background-color:#fff; font-size:26px"><?=$theme->contactus_page_title;?></span></p>

    <hr />
    <p>&nbsp;</p>

    <div class="row">
        <div class="col-md-5 col-sm-12 col-xs-12">
            <!--<h2>Интернет магазин <span>"MY<span class="red">STOCK</span>"</span></h2>
            <div class="contact_text">
                This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.
            </div>-->
            <?=$theme->contactus_page_text;?>
            <div class="contact_info">
                <div class="row">
                        <div class="col-sm-1">
                            <img src="/frontend/web/mt/img/cont_1.png"/>
                        </div>
                        <div class="col-sm-11">
                            <?=$theme->times;?>
                        </div>
                </div>
                <div class="row">
                        <div class="col-sm-1">
                            <img src="/frontend/web/mt/img/cont_2.png" />
                        </div>
                        <div class="col-sm-11">
                            <?=$theme->phone1;?></br>
                            <?=$theme->phone2;?></br>
                        </div>
                </div>
                <div class="row">
                        <div class="col-sm-1">
                            <img src="/frontend/web/mt/img/cont_3.png" />
                        </div>
                        <div class="col-sm-11">
                            <p>
                                <?=$theme->email;?>
                            </p>
                        </div>
                </div>
                 <div class="row">
                        <div class="col-sm-1">
                            <img src="/frontend/web/mt/img/cont_4.png" />
                        </div>
                        <div class="col-sm-11">
                            <p>
                                <?=$theme->adress;?>
                            </p>
                        </div>
                </div>

            </div>
        </div>
        <div class="col-md-7 col-sm-12 col-xs-12 contact_map">
            <?=$theme->contactus_page_map;?>
        </div>
    </div>
</div>