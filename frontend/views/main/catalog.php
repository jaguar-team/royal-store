<?php
use yii\helpers\Url;
use yii\widgets\LinkPager; //виджет для пагиинации что б передавать в скл запрос смещение и лимит
use yii\widgets\Pjax;
$this->title = ($category->seo_title) ? $category->seo_title : "Все товары";

/** breadcrumbs */
$category_breadcrumbs = $category;
$breadcrumbs = true;

while($breadcrumbs) {
    $this->params['breadcrumbs'][] = [
        'label' => $category_breadcrumbs->name,
        'url'   => Url::toRoute(['main/catalog', 'id' => $category_breadcrumbs->id]),
    ];

    $parent = $category_breadcrumbs->parent;
    if ($parent) {
        $category_breadcrumbs = $category_breadcrumbs->find()->where(['id' => $parent])->one();
    } else {
        $breadcrumbs = false;
    }
}
asort($this->params['breadcrumbs']);

$this->registerMetaTag([ 
    'name'=>'description', 
    'content'=>$category->seo_desc
]); 
$this->registerMetaTag([ 
    'name'=>'keywords', 
    'content'=>$category->seo_keys
]);
?>

<div class="container">
    <h1 class="text-center"><?php echo $category->name ?></h1>
    <?php if($category->banner_img){?>
    <div class="catalog" style="text-align: center;">
        <?php if($category->banner_link){?>
             <a href="<?= $category->banner_link ?>" title="" target="_blank" style="width:100%;"><img src="<?php echo Url::to('@web/mt/img/'.$category->banner_img, true); ?>" style="width:100%;"></a>
        <?php } else {?>
             <img src="<?php echo Url::to('@web/mt/img/'.$category->banner_img, true); ?>" style="width:100%;">
        <?php }?>
    </div>
    <?php }?>
    <div class="row">
        <div class="<?= ($category->desc ? 'text' : '') ?> col-lg-12 col-md-12 col-sm-12 hidden-xs">
            <?php if ($category->desc): ?>
                <p><?= $category->desc; ?></p>
                <a href="" id="text-collapse">Читать далее</a>
            <?php else: ?>
                <p>Описание отсутствует</p>
            <?php endif; ?>
        </div>
        <div class="<?= ($category->desc ? 'text' : '') ?> visible-xs col-xs-12">
            <?php if ($category->desc): ?>
                <p class="text-collapse"><?= $category->desc; ?></p>
            <?php else: ?>
                <p>Описание отсутствует</p>
            <?php endif; ?>
        </div>
    </div>
    <div class="row">    
        <div class="col-lg-12 col-md-12 col-sm-12-col-xs-12">
            <div class="filter">
                <div class="title">Фильтровать по: <i class="fa fa-sort-amount-desc visible-xs" aria-hidden="true"></i></div>
                <ul class="hidden-xs">
                	<i class="fa fa-times visible-xs"></i>
                    <?php foreach ($attribute->getAll(false) as $number => $attribute): ?>
                        <li class="hasSub">
                            <a href=""><?= $attribute->name; ?></a>

                            <?php $attributeValues = $attribute->productAttributeValueModel->find()->where(['id_attribute' => $attribute->id])->all(); ?>
                            <div class="float-sub">
                                <?php foreach ($attributeValues as $number_value => $value): ?>
                                    <?php if ($number_value != 0 && $number_value % 4 == 0): ?>
                                        </ul>
                                    <?php endif; ?>
                                    <?php if ($number_value == 0 || $number_value % 4 == 0): ?>
                                        <ul>
                                    <?php endif; ?>
                                    <li>
                                        <a href="<?= Url::toRoute(['main/catalog','id' => $category->id, 'type' => 'attr', 'attr_id' => $attribute->id, 'attr_value_id' => $value->id]); ?>" title="<?= $value->value; ?>"><?= $value->value; ?></a></i>
                                    </li>
                                <?php endforeach; ?>
                            </div>

                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>

        <?php if($products){?>
        <div class="col-md-12 col-sm-12 col-xs-12 catalog_prod">
            <div class="b-items">
                <?php $i = 0; foreach ($products as $prod) {  ?>
                    <div class="b-items-item">
                        <a href="<?= Url::toRoute(['main/product', 'alias' => $prod->alias]); ?>" title="<?= $prod->name; ?>">                           
                            <div  class="img">
                            	<img src="<?php echo Url::to('@web/mt/img/'.$prod->main_img, true); ?>"/>
                        	</div>
	                        <?php if ($prod->discount) { ?>
	                            <div class="discount">- <?= $prod->discount; ?>%</div>
	                        <?php } ?>
	                        
	                        <div class="b-items-bottom">
	                            <div class="title">
	                                <?= $prod->name; ?>
	                            </div>
	                            <div class="prod_price">                  
	                                
                                    <?php if ($prod->discount) { ?>
                                        <strike class="prod_old_price"><?= $prod->price; ?> грн.</strike>
                                    <?php } ?>
                                    <div class="<?php echo ($prod->discount ? 'new' : '')?>"><?= $prod->r_price; ?> грн.</div>
	                            </div>
	                            <div class="links">
	                                <a class="left towish" href="#" onclick="towishlist(event, <?= $prod->id; ?>, '<?= Url::to(Yii::$app->homeUrl, true); ?>')" tabindex="0">
	                                    <span class="save">
                                            <i class="icon icon-heart"></i>
	                                       <span>В избранное</span>
                                        </span>
                                        
	                                </a>
                                    <a href="#" class="fromwish hidden" onclick="delfromwishlist(event, <?= $prod->id; ?>, '<?= Url::to(Yii::$app->homeUrl, true); ?>')">
                                        <span class="remove"><i class="icon icon-heart-broken"></i> Отменить</span>
                                    </a>
	                                <a href="<?= Url::toRoute(['main/product', 'alias' => $prod->alias], true); ?> " title="<?= $prod->name; ?>">
	                                    <i class="icon icon-shoppingbasket"></i>
	                                    <span>Купить сейчас</span>
	                                </a>
	                            </div>
	                        </div>
                    	</a>
                    </div>
                    
                <?php } ?>
            </div>
            <div class="row">
                <div class="col-sm-12 pag">
                    <?php if($pagination) {?>
                        <div id="pages" style="text-align: center;">
                            <?=LinkPager::widget([
                                'pagination'=>$pagination,
                                //'firstPageLabel'=>'В начало',
                                //'lastPageLabel'=>'В конец',
                                'prevPageLabel'=>'<i class="fa fa-angle-left" aria-hidden="true"></i> Назад',
                                'nextPageLabel'=>'Вперед <i class="fa fa-angle-right" aria-hidden="true"></i>'
                            ]);?>
                        </div>
                    <?php }?>
                </div>
            </div>
            <?php } else {?>
            <div class="col-md-9  catalog_prod">
                <div class="alert alert-warning" role="alert">Извините, но пока что нет товаров в этой категории</div>
            </div>
            <?php }?>
        </div>
    </div>
</div>
<div class="modal fade" id="added" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <p class="mod1">Добавленно!</p>
        <button  class="modbutt" data-dismiss="modal" aria-label="Close">Закрыть</button>
      </div>
    </div>
  </div>
</div>