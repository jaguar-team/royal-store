<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;

$this->title = $prod->seo_title;
$this->params['breadcrumbs'][] = ['label' => $prod->category->name, 'url' => ['main/catalog', 'alias' => $prod->category->alias]];
$this->params['breadcrumbs'][] = $this->title;

$this->registerMetaTag([
    'name' => 'description',
    'content' => $prod->seo_desc
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $prod->seo_keys
]);
?>
<style>
    .fotorama img {
        max-width: 458px;
    }
</style>

<div class="container oneproduct">
    <!--<a href="<?= Url::toRoute(['main/catalog', 'alias' => $prod->category->alias]); ?>" class="goback">< Вернуться назад</a>-->
    <div class="row rowprod">
        <div class="col-lg-7 col-md-6 col-sm-6">
            <div class="littprodslider">
                <div class="fotorama" data-allowfullscreen="false" data-maxwidth="525" data-width="100%" data-thumbwidth="113"
                     data-thumbheight="115" data-nav="thumbs" data-arrows="true" data-click="true" data-swipe="true">
                    <a href="<?= Url::to('@web/mt/img/'.$prod->main_img, true); ?>"><img src="<?= Url::to('@web/mt/img/'.$prod->main_img, true); ?>"/></a>
                    <?php if (count($prod->photos) >= 1) {
                        foreach (explode("##", $prod->photos) as $photo) { ?>
                            <a href="<?= Url::to('@web/mt/img/'.$photo, true); ?>"><img src="<?= Url::to('@web/mt/img/'.$photo, true); ?>"/></a>
                        <?php }
                    } ?>
                </div>
            </div>
            <div class="bigprodslider">
                <ul id="etalage">
                    <li>
                        <a href="<?= Url::to('@web/mt/img/'.$prod->main_img, true); ?>">
                            <img class="etalage_source_image" src="<?= Url::to('@web/mt/img/'.$prod->main_img, true); ?>"/>
                            <img class="etalage_thumb_image" src="<?= Url::to('@web/mt/img/'.$prod->main_img, true); ?>"/>
                        </a>
                    </li>
                    <?php if (count($prod->photos) >= 1) {
                        foreach (explode("##", $prod->photos) as $photo) {
                            if ($photo) { ?>
                                <li>
                                    <a href="<?= Url::to('@web/mt/img/'.$photo, true); ?>">
                                        <img class="etalage_source_image" src="<?= Url::to('@web/mt/img/'.$photo, true); ?>"/>
                                        <img class="etalage_thumb_image" src="<?= Url::to('@web/mt/img/'.$photo, true); ?>"/>
                                    </a>
                                </li>
                            <?php }
                        }
                    } ?>
                    <?php if ($prod->discount) { ?>
                        <span class="discount">-<?= $prod->discount; ?>%</span>
                    <?php } ?>
                </ul>
                <div class="hidden"><div id="zoom"></div></div>
                
            </div>
        </div>
        <div class="col-lg-5 col-md-6 col-sm-6 prodinfo">

            <div class="col-sm-112">
                <h1><?= $prod->name; ?></h1>
            </div>
            <div class="prices col-lg-12 col-md-12 col-sm-12">
                <?php if ($prod->discount) { ?>
                    <span class="price full"><?= $prod->price ?> грн.</span>
                    <span class="price sale"><?= $prod->r_price; ?> грн.</span>
                <?php } else { ?>
                    <span class="price"><?= $prod->price; ?> грн.</span>
                <?php } ?>
                <span class="instock">
                    <?php if ($prod->amount > 0) { ?>
                        В наличии
                    <?php } else { ?>
                        Нет в наличии
                    <?php } ?>
                </span>
            </div>
            <div class="purchase">
                <select class="onesize" name="id" style="display: none;">
                	<option value="-1" disabled="disabled" selected="selected">Выберите размер</option>
                	<?php if (!empty(explode(", ", $prod->sizes))): ?>
	                	<?php $i = 0; foreach (explode(", ", $prod->sizes) as $size): ?>
		                    <option value="<?= $size; ?>" data-id="<?= $i ?>">
		                        <?=$size;?>
		                    </option>
	                	<?php $i++; endforeach; ?>
	                <?php endif; ?>	
                </select>
                <div id="id-slct" class="slct" tabindex="0">
                    <div class="status"><span>Choose variant</span>
                        <i class="icon"></i>
                    </div>
                    <ul class="list">                        
                    </ul>
                </div>
                <a class="buy" href="<?= Url::toRoute(['cart/addtocart', 'id' => $prod->id]); ?>&size=">Купить</a>
            </div>

            <div class="product-actions">
                <div class="action favorite">
                    <a href="" class="towish" onclick="towishlist(event, <?= $prod->id; ?>, '<?= Url::to(Yii::$app->homeUrl, true); ?>')">
                        <span class="save"><i class="icon icon-heart-small"></i>  В избранное</span>
                    </a>
                    <a href="" class="fromwish hidden" onclick="delfromwishlist(event, <?= $prod->id; ?>, '<?= Url::to(Yii::$app->homeUrl, true); ?>')">
                        <span class="remove"><i class="icon icon-heart-broken-small"></i> Отменить</span>
                    </a>
                </div>               
                                                
                <div class="action comment">
                    <a href="#comment" data-disqus-identifier="/mllotte-woven-pant-black/p/146651">
                        <i class="icon icon-comments"></i> Комментарии
                    </a>
                </div>
            </div>

            <div class="description">
                <?= $prod->desc; ?>
            </div>

            <ul class="product-properties">        
                    
                <li class="item-no">
                    Код товара: <?= $prod->code; ?>
                </li>
            </ul>

        </div>
    </div>
</div>

<div class="container">
    <?php Pjax::begin();?>
        <!-- comment -->
        <div class="product-comment-form" id="comment">
            <div class="title">
                <span>0 Комментариев</span>
            </div>
            <div class="actions">
                <ul>
                    <li>
                        <div class="thread-likes">
                            <a href="#" id="recommend" title="Recommend this discussion" class="dropdown-toggle upvoted">
                                <span class="icon-heart"></span>
                                <span class="recommend">Рекомендовать</span>
                                <span class="recommended hidden">Рекомендуется</span>
                                <span class="label label-count hidden">1</span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu--coachmark">
                                <li>
                                    <h2 class="coachmark__heading">Обсуждение рекомендовано!</h2>
                                    <p class="coachmark__description">Рекомендация означает, что этим обсуждением стоит поделиться.</p>
                                    <a href="https://disqus.com/home/?utm_source=disqus_embed&amp;utm_content=recommend_btn" class="btn btn-primary coachmark__button" target="_blank">
                                        Найти больше обсуждений
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li id="thread-share-menu" class="nav-tab nav-tab--secondary dropdown share-menu hidden-sm" data-dropdown="enabled">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Share" data-tid="74">
                            <span class="icon-export"></span>
                            <span class="share">Поделиться</span>
                        </a>
                        <ul class="share-menu dropdown-menu">
                            <li>
                                <div class="share-menu__label">Поделиться обсуждением в:</div>
                                <ul>
                                    <li class="twitter">
                                        <a data-action="share:twitter" href="#">Twitter</a>
                                    </li>
                                    <li class="facebook">
                                        <a data-action="share:facebook" href="#">Facebook</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true]]);?>
                    <?= $form->field($comment, 'id_product')->hiddenInput(['value' => $prod->id])->label(false) ?>

                    <?= $form->field($comment, 'comment')->textarea(['placeholder' => 'Введите ваш комментарий', 'class' => 'form-control'])->label(false) ?>
    
                    <?= $form->field($comment, 'name')->textInput(['placeholder' => 'Введите ваше имя', 'class' => 'form-control'])->label(false) ?>
    
                    <div class="form-group">
                        <?= Html::submitButton('<span class="icon-proceed"></span>', ['class' => 'proceed__button']) ?>
                    </div>
            <?php ActiveForm::end(); ?>
        </div>

        <?php if ($comment_list = $comment->getCommentsByProductId($prod->id)): ?>
            <div class="product-comment-list">
                <ul>
                    <?php foreach ($comment_list as $number => $comment): ?>
                        <li>
                            <div class="comment-header">
                                <span class="author"><?= $comment->name; ?></span>
                                <!--<span class="bullet time-ago-bullet" aria-hidden="true">•</span>
                                <span class="time">час назад</span>-->
                            </div>
                            <div class="comment-body">
                                <?= $comment->comment; ?>
                            </div>
                            
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php else: ?>
            <div class="alert alert-warning" role="alert">Извините, но пока что нет комментариев для этого товара.</div>
        <?php endif; ?>
        <!-- /comment -->
    <?php Pjax::end();?>

</div>

<div class="modal fade" id="added" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <p class="mod1">Добавленно!</p>
                <button class="modbutt" data-dismiss="modal" aria-label="Close">Закрыть</button>
            </div>
        </div>
    </div>
</div>