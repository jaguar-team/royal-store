<?php
use yii\helpers\Url;
use yii\widgets\LinkPager; 
$this->title = "Поиск товаров MY STOCK $q";
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 style="text-align: center;">Результаты поиска <i><?=$q;?></i>: </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 catalog_prod">
            <div class="row">
            <?php if(!$products || !$q){?>
                <div style="margin:50px;" class="alert alert-warning" role="alert">Товаров по Вашему запросу не найдено</div>
            <?php } else {?>
            <?php foreach($products as $prod) { ?>
                <div class="col-md-4 col-sm-6 col-xs-12 prod_slider">
                    <div class="prod">
                        <?php if($prod->discount) {?>
                            <span class="label label-default prod_label">- <?= $prod->discount;?>%</span>
                        <?php } ?>
                        <h3 <?php if(!$prod->discount) {?>style="padding-top: 39px; margin-top: 0;"<?php } ?>><a href="<?=Url::toRoute(['main/product', 'alias' => $prod->alias]);?>"><?= $prod->name;?></a></h3>
                        <a href="<?=Url::toRoute(['main/product', 'alias' => $prod->alias]);?>"><img src="/frontend/web/mt/img/<?= $prod->main_img;?>" alt="<?= $prod->name;?>" /></a>
                        <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-6 prod_col1">
                                <div class="prod_code">Код товара: <?= $prod->code;?></div>
                                <?php if($prod->discount) {?>
                                    <div class="prod_old_price"><?= $prod->price;?> грн.</div>
                                    <div class="prod_new_price">Цена: <?= $prod->r_price;?> грн.</div>
                                <?php } else { ?>
                                    <div style="margin-top: 32px;" class="prod_new_price">Цена: <?= $prod->r_price;?> грн.</div>
                                <?php } ?>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6 prod_col2">
                                <div class="prod_fav">
                                    <a href="#">
                                        <i class="fa fa-heart-o" aria-hidden="true"></i>
                                        <span>В избранное</span>
                                    </a>
                                </div>
                                <a class="prod_button" href="<?=Url::toRoute(['main/product', 'alias' => $prod->alias]);?>">Купить</a>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            <?php } }?>  
            </div>
            <div class="row">
                <div class="col-sm-12 pag">
                    <?php if($pagination) {?>
                        <div id="pages" style="text-align: center;">
                            <?=LinkPager::widget([
                                'pagination'=>$pagination,
                                //'firstPageLabel'=>'В начало',
                                //'lastPageLabel'=>'В конец',
                                'prevPageLabel'=>'<i class="fa fa-angle-left" aria-hidden="true"></i> Назад',
                                'nextPageLabel'=>'Вперед <i class="fa fa-angle-right" aria-hidden="true"></i>'
                            ]);?>
                        </div>
                    <?php }?>
                </div>
            </div>
        </div>
    </div>
</div>