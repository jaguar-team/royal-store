<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = $theme->seo_main_title;

$this->registerMetaTag([
    'name' => 'description',
    'content' => $theme->seo_main_desc
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $theme->seo_main_keys
]);
?>
<div class="container">
        <div class="phones visible-xs">
            <a href="tel:<?= $theme->phone1; ?>"><?= $theme->phone1; ?></a>
            <a href="tel:<?= $theme->phone1; ?>"><?= $theme->phone2; ?></a>
        </div>
    <?php if ($slider): ?>        
        <div class="banner" id="banner">
            <?php $i=0; foreach ($slider as $number => $slide): ?>
                <div class="item <?php echo ($i==0 ?  'active' : '') ?>" style="background-image: url('<?= Url::to('@web/mt/img/'.$slide->img, true); ?>');">                        
                </div>
            <?php $i++; endforeach; ?>
        </div>
    <?php endif; ?>
        
    <!--<div class="indexTab Top">

        <div class="row back_fon">
            <div class="col-md-12">
                <div class="strip">
                    <h2>Отличники StoreKids</h2>
                </div>
            </div>
        </div>

        <div class="owl-carousel">
            <?php $i = 0; foreach ($top as $prod) { ?>
            <div class="b-items-item">
                    <a href="<?= Url::toRoute(['main/product', 'alias' => $prod->alias]); ?>" title="" class="img">
                        <img src="<?php echo Url::to('@web/mt/img/'.$prod->main_img, true); ?>"/>
                    </a>
                    <?php if ($prod->discount) { ?>
                        <div class="discount">- <?= $prod->discount; ?>%</div>
                    <?php } ?>
                    
                    <div class="b-items-bottom">
                        <div class="title">
                            <a href="<?= Url::toRoute(['main/product', 'alias' => $prod->alias]); ?>" title="">
                                <?= $prod->name; ?>                                
                            </a>
                        </div>
                        <?php if ($prod->discount) { ?>
                            <strike class="prod_old_price"><?= $prod->price; ?> грн.</strike>
                        <?php } ?>
                        <div class="prod_price <?php echo ($prod->discount ? 'new' : '')?>"><?= $prod->r_price; ?> грн.</div>
                        <div class="links">
                            <a href="#" onclick="towishlist(event, <?= $prod->id; ?>)" tabindex="0">
                                <i class="icon icon-heart"></i>
                                <span>В избранное</span>
                            </a>
                            <a href="<?= Url::toRoute(['main/product', 'alias' => $prod->alias], true); ?>">
                                <i class="icon icon-shoppingbasket"></i>
                                <span>Купить сейчас</span>
                            </a>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div> -->

    <?php if ($categories): ?>
        <div class="categories">
            <?php foreach ($categories as $number => $category): ?>
                <?php if ($category->parent === 0): ?>
                    <div class="item">
                        <a href="<?= Url::toRoute(['main/catalog', 'id' => $category->id]); ?>" title="<?= $category->name; ?>">
                            <div class="img">
                                <img src="<?php echo Url::to('@web/mt/img/'.$category->cat_img, true); ?>">
                            </div>
                            <div class="content">
                                <h4><?= $category->name; ?></h4>
                                <div class="desc">Финальная распродажа</div>
                            </div>
                        </a>    
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>    

</div>

<div class="modal fade" id="added" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <p class="mod1">Добавленно!</p>
                <button class="modbutt" data-dismiss="modal" aria-label="Close">Закрыть</button>
            </div>
        </div>
    </div>
</div>
