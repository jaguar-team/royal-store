<?php

/* @var $this \yii\web\View */
/* @var $content string */

use common\models\Category;
use common\models\Contact;
use common\models\Product;
use common\models\Newsletter;
use frontend\models\Search;
use common\models\ThemeSettings;
use common\widgets\Alert;
use frontend\assets\MyAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\widgets\ActiveForm;

$theme = ThemeSettings::findOne(['id' => 1]);
$search = new Search();

/** send email footer **/
$newsletter = new Newsletter();
if ($newsletter->load(Yii::$app->request->post())) {
    if (Yii::$app->mailer->compose()
        ->setTo($theme->email)
        //->setFrom('localhost@support.com')
        ->setSubject('Новостная подписка с сайта store-kids.com.ua')
        ->setHtmlBody('Email пользователя: <b>'.$newsletter->email.'<b>')
        ->send()) {
        $newsletter->email = '';
    }
}

/** wishlist */
$ids=Yii::$app->request->cookies->getValue('wishlist');
$products=(new Product())->findProductsByOrderIds($ids);

MyAsset::register($this);
$model = new Contact();

$categories = (new Category)->getAll();
?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="container-fluid static-menu hidden-xs">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">           
            <ul class="nav navbar-nav ">
                <li><a href="<?= Url::toRoute(['main/about']); ?>">О нас</a></li>
                <li><a href="<?= Url::toRoute(['main/wishlist']); ?>">Избранное</a></li>
                <li><a href="<?= Url::toRoute(['main/delivery']); ?>">Оплата и доставка</a></li>
                <li><a href="<?= Url::toRoute(['main/return']); ?>">Возврат и обмен</a></li>
                <li><a href="<?= Url::toRoute(['main/contacts']); ?>" class="last">Контакты</a></li>
            </ul>
        </div>
    </nav>
</div>
<div class="header hidden-xs">
    <div class="container">
        <div class="row">
            <div class="logo_header col-lg-2 col-md-2 col-sm-5 col-xs-12">
                <a title="<?= $theme->shop_name; ?>" href="<?= Url::toRoute(['main/index'], true); ?>">
                    <img src="<?= Url::to('@web/mt/img/'.$theme->main_photo, true); ?>"/>
                </a>
            </div>

            <div class="search col-lg-4 col-md-4 col-sm-7 col-xs-12">
                <?php $form = ActiveForm::begin(['id' => 'form-search','enableClientValidation'=>false,'enableAjaxValidation'=>false,'options'=>['class'=>'form_search']]);?>

                    <?= $form->field($search, 'q')->textInput(['placeholder'=>'Поиск', 'class'=>'search_main', 'id' => 'search-q'])->label(false) ?>

                    <button type="submit" class="btn btn-turqouise"><i class="icon icon-search"></i></button>
                <?php ActiveForm::end(); ?>
            </div>
         
            <div class="phones col-lg-2 col-md-2 col-sm-6 col-xs-12">
                <a href="tel:<?= $theme->phone1; ?>"><?= $theme->phone1; ?></a>
                <a href="tel:<?= $theme->phone2; ?>"><?= $theme->phone2; ?></a>
            </div>          

            
            <div class="cart_header col-lg-3 col-md-4 col-sm-6 col-xs-12 ">         
                
                <div class="float_cart">                    
                    <a href="<?= Url::toRoute(['cart/index'], true); ?>" class="info">
                        <span>Ваша корзина<?php Yii::$app->session->get('count') + 0; ?></span>
                        <span id="k_count"><i class="icon icon-shoppingbasket"></i></span>
                        <span id="k_price"><?= Yii::$app->session->get('price') + 0; ?> грн.</span>
                     </a>
                     <a href="<?= Url::toRoute(['main/wishlist'], true); ?>" class="favorite">
                        <i class="icon icon-heart"></i>
                        <span class="quantity"><?= count($products); ?></span>
                    </a>  
                </div>
            </div>
        </div>
    </div>
</div>
<div class="mobile-header visible-xs">
        <div class="left">
            <a href="" id="menu-toggle" class="icon-menu side-bar-toggler">
                <i class="fa fa-bars"></i>
            </a>
            <a href="" id="search-toggle" class="icon icon-search"></a>
            <a href="<?= Url::toRoute(['main/index'], true); ?>" class="logo">
                <img src="<?= Url::to('@web/mt/img/logo_header_v3.png', true); ?>"/>
            </a>
        </div>
        <div class="right">            
            <a id="mini-cart" class="icon-cart empty" href="/cart/view">
                    <i class="icon icon-shoppingbasket"></i>
                    <span class="quantity">0</span>
            </a>
            <a href="<?= Url::toRoute(['main/wishlist'], true); ?>" class="favorite">
                <i class="icon icon-heart"></i>
                <span class="quantity"><?= count($products); ?></span>
            </a>  
        </div>
        <div class="search">
            <?php $form = ActiveForm::begin(['id' => 'form-search','enableClientValidation'=>false,'enableAjaxValidation'=>false,'options'=>['class'=>'form_search']]);?>

                <a href="" id="search-close" class="btn"><i class="fa fa-close"></i></a>

                <?= $form->field($search, 'q')->textInput(['placeholder'=>'Поиск', 'class'=>'', 'id' => 'search-q'])->label(false) ?>

                <button type="submit" class="btn"><i class="icon icon-search"></i></button>
            <?php ActiveForm::end(); ?>
        </div>
        
</div>
<div class="main_menu hidden-xs">
    <nav class="navbar navbar-inverse">
        <div class="container">
        <?php if ($categories): ?>
                <ul class="nav navbar-nav">
                    <?php foreach ($categories as $number => $category): ?>
                        <?php if (!$category->parent && $category->parent == 0): ?>
                            <li>
                                <a href="<?= Url::toRoute(['main/catalog', 'id' => $category->id]); ?>" title="<?= $category->name; ?>">
                                    <?= $category->name; ?>
                                </a>
                                <div class="subcategory">
                                    <div class="container">
                                        <div class="row">
                                            <div class="title col-lg-12">Категория  <a href="<?= Url::toRoute(['main/catalog', 'id' => $category->id]); ?>">Посмотреть все: <?= $category->name; ?></a></i></div>
                                            <?php
                                                $children = $category->getChildren();
                                            ?>
                                            <?php if ($children): ?>
                                                <?php foreach ($children as $number_child => $child): ?>
                                                    <?php if ($number_child != 0 && $number_child % 4 == 0): ?>
                                                        </ul>
                                                    <?php endif; ?>
                                                    <?php if ($number_child == 0 || $number_child % 4 == 0): ?>
                                                        <ul class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                    <?php endif; ?>
                                                            <li>
                                                                <a href="<?= Url::toRoute(['main/catalog', 'id' => $child->id]); ?>" title="<?= $child->name; ?>"><?= $child->name; ?></a></i>
                                                            </li>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </div>
    </nav>
    
</div>
<div class="breadfon">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
    </div>
</div>

<?= $content ?>

<footer>
    <div class="foot_top">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="title">
                        <?= $theme->shop_name; ?>
                    </div>
                    <div class="inform_text"><?= $theme->main_text; ?>
                        <a href="<?= Url::toRoute(["main/about"]); ?>">Читать далее</a>
                    </div>
                </div>
                <div class="links col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <ul>
                        <li><a href="<?= Url::toRoute(['main/about']); ?>">О нас</a></li>
                        <li><a href="<?= Url::toRoute(['main/wishlist']); ?>">Избранное</a></li>
                        <li><a href="<?= Url::toRoute(['main/delivery']); ?>">Оплата и доставка</a></li>
                        <li><a href="<?= Url::toRoute(['main/return']); ?>">Возврат и обмен</a></li>
                        <li><a href="<?= Url::toRoute(['main/contacts']); ?>">Контакты</a></li>
                    </ul>
                </div>
                <div class="info col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <ul>
                        <li>Тел: <?= $theme->phone1; ?></li>
                        <li style="padding-left: 28px;"><?= $theme->phone2; ?></li>
                        <li><?= $theme->times; ?></li>
                        <li>
                            E-mail: <a href="mailto:<?= $theme->email; ?>"><?= $theme->email; ?></a>
                        </li>
                    </ul>                   
                </div>
                <div class="follow col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <p>
                        Узнать первым о новинках и акциях <br> <?= $theme->shop_url; ?>
                    </p>

                        <?php $form = ActiveForm::begin(['id' => 'form-follow','options'=>['class'=>'form_follow']]);?>

                            <?= $form->field($newsletter, 'email')->textInput(['placeholder'=>'Ваш email', 'class'=>'follow_main', 'id' => 'search-q'])->label(false) ?>

                            <button type="submit" class="btn">ОК</button>
                        <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>


    <div class="mobile-menu">
        <ul>
            <?php $am = count($categories);
            $i = 1;                    
            foreach ($categories as $key => $c) { ?>
                <?php if(!$c->parent): ?>
                <li>
                    <a href="<?= Url::toRoute(['main/catalog', 'id' => $c->id]); ?>" title="<?= $c->name; ?>">
                        <?= $c->name; ?>
                    </a>
                </li>
                <?php endif; ?>
            <?php $i++;
            } ?>
        </ul>
    </div>


</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
