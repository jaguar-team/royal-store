<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use yii\widgets\Pjax;
$this->title = 'Заказ '.$theme->shop_name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container cart">
    <div class="row titleh1">
        <div class="col-sm-12">
            <hr />
            <h1>Заказ</h1>
        </div>
    </div>
    <div class="row steps">
        <div class="col-sm-12">
            <div class="step3"><a href="<?=Url::toRoute(['cart/index']);?>">1 Корзина</a></div>
            <div class="step4">2 Личные данные</div>
        </div>
    </div>
    <div class="clear"></div>
    <div class="row contentorder">
        <?php //Pjax::begin();?>
        <div class="col-md-8">
        <?php $form = ActiveForm::begin(/*['options' => ['data-pjax' => '1']]*/); ?>
            <div class="row formblock">
                <div class="col-sm-6">
                    <?= $form->field($model, 'name')->textInput(['placeholder' => 'Ваше ФИО']) ?>
                    <?= $form->field($model, 'phone')->textInput(['placeholder' => 'Ваш телефон']) ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'city')->textInput(['placeholder' => 'Ваш город', 'data-href' => Url::to(['/cart/getoffice'], true)]); ?>
                    

                    <?= $form->field($model, 'ofice')->dropDownList(
                        $params = [
                        'prompt' => 'Выберите отделение...'
                        ]
                    ); ?>

                </div>
            </div>
            <div class="form-group o_form_butt">
                <?= Html::submitButton('Отправить', [ 'name' => 'contact-button']) ?>
            </div>
            <div class="clear"></div>
        <?php ActiveForm::end(); ?>
        <?php
if($thanks == 1) 
{
    $this->registerJs(' 
    $(document).ready(function() { 
    $("#thanks").modal("show"); 
    });');    
}
?>
<div class="modal fade" id="thanks" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <p class="mod1">Спасибо!</p>
        <p class="mod2">Ваша заявка принята</p>
        <p class="mod3">Менеджер свяжется с вами в ближайшее время</p>
        <p class="mod4">С уважением, команда "Store Kids"</p>
        <button data-href="<?= Url::to('@web/cart/unset', true); ?>" class="modbutt" data-dismiss="modal" aria-label="Close">Закрыть</button>
      </div>
    </div>
  </div>
</div>
        <?php //Pjax::end();?>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="orderinfo">
                <p class="order_title">Ваш заказ:</p>
                <p class="order_desc"><?=Yii::$app->session->get('count');?> товара на сумму: <span><?=Yii::$app->session->get('price');?> грн.</span></p>
                <hr />
                <p class="order_itogo">Итого: <span><?=Yii::$app->session->get('price');?> грн.</span></p>
            </div>
        </div>
    </div>
</div>