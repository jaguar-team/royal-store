<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Корзина '.$theme->shop_name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container cart">
    <div class="row titleh1">
        <div class="col-sm-12">
            <hr />
            <h1>Корзина</h1>
        </div>
    </div>
    <?php if($ids) {?>
    <div class="row steps">
        <div class="col-sm-12">
            <div class="step1">1 Корзина</div>
            <div class="step2"><a href="<?=Url::toRoute(['cart/order']);?>">2 Личные данные</a></div>
        </div>
    </div>
    <div class="clear"></div>
    <div class="row">
        <div class="col-sm-12">
            <div class="table-responsive">
              <table class="table prodtable"> 
                <thead> 
                    <tr> 
                        <th>Фото</th> 
                        <th>Товар</th> 
                        <th>Цена</th> 
                        <th>Количество</th> 
                        <th>Сумма</th> 
                        <th></th> 
                    </tr> 
                </thead> 
                <tbody> 
                    <?php $i=0; foreach($products as $prod){?>
                    <tr> 
                        <td><a href="<?=Url::toRoute(['main/product', 'alias'=>$prod->alias]);?>"><img src="/frontend/web/mt/img/<?=$prod->main_img;?>"/></a></td> 
                        <td>
                            <div class="cartinfo">
                                <h4><a href="<?=Url::toRoute(['main/product', 'alias'=>$prod->alias]);?>"><?=$prod->name;?></a></h4>
                                <span class="code">Код товара: <?=$prod->code;?></span>
                                <span class="size">РАЗМЕР: <?=$ids[$i]['size'];?></span>
                            </div>
                        </td> 
                        <td class="price"><?=$prod->r_price;?> грн.</td> 
                        <td>
                            <div class="counter">
                                <span class="minus" onclick="cartminus(<?=$prod->id;?>, '<?=$ids[$i]['size'];?>', <?=$i;?>, '<?= Url::to(Yii::$app->homeUrl, true); ?>')">-</span>
                                <span id="c_amount<?=$i;?>" class="count"> <?=$ids[$i]['count'];?> </span>
                                <span class="plus" onclick="cartplus(<?=$prod->id;?>, '<?=$ids[$i]['size'];?>', <?=$i;?>, '<?= Url::to(Yii::$app->homeUrl, true); ?>')">+</span>
                            </div>    
                        </td> 
                        <td id="stoimost<?=$i;?>" class="price"><?=$prod->r_price*$ids[$i]['count'];?> грн.</td> 
                        <td><a href="<?=Url::toRoute(['cart/delcart', 'id'=>$prod->id, 'size'=>$ids[$i]['size']]);?>"><span class="glyphicon glyphicon-remove del" aria-hidden="true"></span></a></td> 
                    </tr>
                    <?php $i++; }?>
                </tbody> 
              </table>
            </div>
        </div>
    </div>
    <div class="row totalcart tcm">
        <div class="col-sm-12">
            <hr />
            <p class="itogo">Итого</p>
            <div class="clear"></div>
            <p class="counttext"><span id="tot_count"><?=Yii::$app->session->get('count');?></span> товара на сумму:</p>
            <p class="price" id="tot_sum"><?=Yii::$app->session->get('price');?> грн.</p>
        </div>
    </div>
    <div class="row totalcart">
        <div class="col-lg-2 col-md-3 col-sm-4 cont">
            <a href="<?=Url::toRoute(['main/catalog']);?>">Продолжить покупки</a>
        </div>
        <div class="col-lg-2 col-lg-offset-8 col-md-3 col-md-offset-6 col-sm-4 col-sm-offset-4 toorder">
            <a href="<?=Url::toRoute(['cart/order']);?>">Оформить заказ</a>
        </div>
    </div>
    <?php } else {?>
        <div class="row">
            <div class="alert alert-danger" style="text-align:center;" role="alert">В корзине пока что нету товаров...</div>
        </div>
    <?php }?>
</div>