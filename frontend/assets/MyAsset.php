<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class MyAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [                
        'mt/css/font-awesome.min.css',
        'mt/css/fancybox.min.css',
        'mt/css/fotorama.css',
        'mt/css/slick.css',
        'mt/css/slick-theme.css',        
        'mt/css/etalage.css',
        'mt/css/owl.carousel.css',
        //'mt/css/glasscase.min.css',
        //'mt/css/default.css'
        'mt/css/main.css',
        'mt/css/index.css',
        'mt/css/cart.css',
        'mt/css/catalog.css',
        'mt/css/wishlist.css',
    ];
    public $js = [      
        'mt/js/jquery.swipe.min.js',  
        'mt/js/fancybox.min.js',
        'mt/js/fotorama.js',
        'mt/js/slick.min.js',
        'mt/js/jquery.etalage.min.js',
        'mt/js/owl.carousel.min.js',
        'mt/js/scripts.js',
        //'mt/js/easyzoom.min.js',
        //'mt/js/jquery.glasscase.min.js',
        //'mt/js/modernizr.custom.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
