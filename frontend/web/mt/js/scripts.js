$(document).ready(function () {

    $(window).scroll(function(){
        Menu();
    });
    $(window).resize(function(){
        $('.b-items-item .img').css({'height': 1.2 * $('.b-items-item .img').outerWidth()});
    });

    $('.b-items-item .img').css({'height': 1.2 * $('.b-items-item .img').outerWidth()});

    
    $('.filter > ul > li.hasSub > a').click(function(e){
        e.preventDefault();
        var sub = $(this).parent().find('.float-sub');
        if(!sub.hasClass('open')){
            if($('.float-sub').hasClass('open')){
                $('.float-sub').hide().removeClass('open');
                sub.show();
            }
            else{
                sub.css({'width': (sub.find('ul').length * 250)});
                sub.slideDown();                
            } 
            sub.addClass('open');           
        }
        else {
            sub.hide(); 
            sub.removeClass('open');
        }
        
    });

    $('#menu-toggle').click(function(e){
        e.preventDefault();
        if(!$('.mobile-menu').hasClass('open')){
            $(this).html('<i class="fa fa-times"></i>');
        }
        else{
            $(this).html('<i class="fa fa-bars"></i>');
        }
        $('.mobile-menu').toggleClass('open');
        
    });
    $('#search-toggle').click(function(e){
        e.preventDefault();
        $('.mobile-header .search').toggleClass('open');
    });
    $('#search-close').click(function(e){
        e.preventDefault();
         $('.mobile-header .search').toggleClass('open');
         e.stopPropagation();
    });
    /*$('.mobile-header .menu ul > li > a').click(function(e){
        e.preventDefault();
        $(this).parent().find('.subcategory').slideDown();
    });*/
    $('.main_menu .navbar-inverse .navbar-nav > li').on({
        mouseenter: function(){
            var $this = $(this);
            var sub = $(this).find('.subcategory');            
            $('.main_menu .subcategory').hide();
            sub.show();
            $this.parent().children().removeClass('active');
            $this.addClass('active');

        },
        mouseleave: function(){
            var flag = false;
            var $this = $(this);
            var sub = $(this).find('.subcategory');
            sub.mouseenter(function(){
                flag = true;
            });    
            setTimeout(function(){
                if(!flag)  {
                    sub.hide();
                    $this.removeClass('active');
                }
                else return;
            }, 75);
        }
    });

    $('#text-collapse').on("click", function (e) {
        e.preventDefault();    
        $(this).parent().find('p').toggleClass('open');    
        if($(this).parent().find('p').hasClass('open')){
            $(this).html('Свернуть');
        }
        else $(this).html('Читать далее');
        
    });
    $('.text-collapse').click(function(){
        $(this).css({'height' : 'auto'});
        $(this).removeClass('text-collapse');
    });

    $('.filter i').click(function(){
        var ul = $(this).closest('.filter').find('ul:first')
        //console.log(ul);
        ul.toggleClass('hidden-xs');
    });
    $('.filter > ul > li.hasSub > a').click(function(e){
        e.preventDefault();
        $(this).parent().find
    });

    $("#etalage").etalage({
        thumb_image_width: $('#etalage').parent().outerWidth() * .8,
        thumb_image_height: $('#etalage').parent().outerWidth(),
        source_image_width: 1017,
        source_image_height: 1017,
        nextClick: true,
        //zoom_area_width: $('#etalage').parent().outerWidth() / 1.3,
        //zoom_area_height: $('#etalage').parent().outerWidth() / 1.3,
        autoplay: false,
        smallthumbs_position: 'left',
        small_thumbs: 7,
        zoom_element: '#zoom',
        magnifier_opacity: 1,
        click_callback: function(image_anchor, instance_id){
		$.fancybox({
			autoSize: false,
			minWidth: 800,
			minHeight: 1000,
			href: image_anchor,

		});
	}
    });

   	

    $('#etalage').find('.etalage_thumb').append(`
        <a class="button-prev slidebigprodprev"><i class="fa fa-caret-left" aria-hidden="true"></i></a>
        <a class="button-next sliderbigprodnext"><i class="fa fa-caret-right" aria-hidden="true"></i></a>
        `);

    
    $(".slidebigprodprev").on("click", function () {
        etalage_previous();
    });
    $(".sliderbigprodnext").on("click", function () {
        etalage_next();
    });
    
    $('.owl-carousel').owlCarousel({
        nav: true,
    	loop: true,
    	dots: false,
        touchDrag: true,
        mouseDrag: true,
        responsive : {
            0 :{
                items: 1,
                nav: false,
            },
            480 : {
                items: 2,
            },
            768 : {
                items: 3,
            },
            991 : {
                nav: true,
            },
            1200 : {
                items: 4,
            }
        }
    });
    $( ".owl-prev").html('<span class="prev glyphicon glyphicon-chevron-left"></span>');
    $( ".owl-next").html('<span class="next glyphicon glyphicon-chevron-right"></span>');

    $('#banner').slick({
        dots: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        accessibility: true,
        nextArrow: '<span class="next glyphicon glyphicon-chevron-right"></span>',
        prevArrow: '<span class="prev glyphicon glyphicon-chevron-left"></span>',
        responsive: [{
            breakpoint: 880,
            settings: {
                arrows: false
            }
        }]       
    });
    
});
$(document).ready(function () {
    $(".responsive").slick({
        dots: false,
        infinite: false,
        speed: 200,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [{breakpoint: 1024, settings: {slidesToShow: 3, slidesToScroll: 3, infinite: true, dots: false}}, {
            breakpoint: 600,
            settings: {slidesToShow: 2, slidesToScroll: 2, infinite: true, dots: false}
        }, {breakpoint: 480, settings: {slidesToShow: 1, slidesToScroll: 1, infinite: true, dots: false}}]
    });
});
$(document).ready(function () {

    $('#recommend').click(function(e){
        e.preventDefault();
        $(this).find('.recommend').toggleClass('hidden');
        $(this).find('.recommended').toggleClass('hidden');
        $(this).find('.label').toggleClass('hidden');
        $(this).find('.icon-heart').toggleClass('red');
    });

    $('textarea').on('focus', function(){
        $(this).addClass('full');
        $(this).parent().next().find('input[type="text"]').show(50);
    });

    $('input[type="text"]').on('focus', function(){
        $(this).closest('form').find('.proceed__button').show(50);
    });

    $('.reply').on('click', function(e){
        e.preventDefault();
        $(this).toggleClass('active');
        $(this).closest('li').find('.reply-form').toggleClass('hidden');
        $(this).closest('li').find('.reply-form').find('textarea').focus();
    });

	$('a[href^="#"').click(function(){

		var el = $($(this).attr('href'));
		$('html, body').animate({
            scrollTop: el.offset().top,
        }, 700);
		return false; 
	});


    $(".onesize").on("click", function () {
        $("#err_block").css("display", "none");
        var arr = $(".onesize");
        jQuery.each(arr, function (i, val) {
            arr.css("background", "#fff");
            arr.css("color", "#000");
        });
        $(this).css("background", "#000").css("color", "#fff");
        var jadd = $("#jadd").attr("href");
        $("#jadd").attr("href", jadd.split("&")[0] + "&size=K");
    });
    $("#jadd").on("click", function (e) {
        if (($("#jadd").attr("href")).indexOf("size") == -1) {
            e.preventDefault();
            $("#err_block").css("display", "block");
        }
    });
    $("#orderform-city").change(function () {
        var city = $(this).val();
        //console.log($(this).attr('data-href'));
        $.get($(this).attr('data-href'), {city: city}, function (data) {
            var data = $.parseJSON(data);
            var new_options = "<option value=''>Отделение Новой почты</option>";
            $.each(data, function (key, value) {
                new_options += "<option value='" + key + "'>" + value + "</option>";
            });
            $("#orderform-ofice").html(new_options);
        });
    });
    $(function(){
    	var list = $('#id-slct').find('ul.list');
        var link = $('a.buy');
        var select = $('#id-slct').parent().find('select');
    	select.find('option').each(function(){

    		list.append('<li class="option" data-id="' + $(this).attr('data-id') + '">' + $(this).text() + '</li>');

    	});

        link.attr('href', link.attr('href') + select.val());

    	list.find('li').on('click', function(){

			$('#id-slct .status > span').text($(this).text());
			select.find('option').removeAttr('selected');
			select.find('option[data-id = "' + $(this).attr('data-id') + '"]').attr('selected', 'selected');
            
			var str = link.attr('href');
            var size = str.split('&');
            
            $('a.buy').attr('href', str.replace(size[1].split('=')[1], select.val()));
		});
    	$('#id-slct .status > span').text(list.find('.option:first-of-type').text());

    });
    $('#id-slct').click(function(){
		$(this).find('ul.list').toggleClass('open');
    });
});





function cartplus(id, size, i, url) {
    $.get(url + "cart/pluscart", {id: id, size: size, i: i}, function (data) {
        var data = $.parseJSON(data);
        //$("#k_count").text(data.count);
        $("#tot_count").text(data.count);
        $("#k_price").text(data.price);
        $("#k_count_lit").text(data.count);
        $("#k_price_lit").text(data.price);
        $("#tot_sum").text(data.price + " грн.");
        $("#c_amount" + i).text(data.onecount);
        $("#stoimost" + i).text(data.onecount * data.oneprice + " грн.");
    });
}
function cartminus(id, size, i, url) {
    $.get(url + "cart/minuscart", {id: id, size: size, i: i}, function (data) {
        var data = $.parseJSON(data);
        //$("#k_count").text(data.count);
        $("#tot_count").text(data.count);
        $("#k_price").text(data.price);
        $("#k_count_lit").text(data.count);
        $("#k_price_lit").text(data.price);
        $("#tot_sum").text(data.price + " грн.");
        $("#c_amount" + i).text(data.onecount);
        $("#stoimost" + i).text(data.onecount * data.oneprice + " грн.");
    });
}
$("#thanks .modbutt").on("click", function () {
    //console.log($(this).attr('data-href'));
    document.location.href = $(this).attr('data-href');

});
$('.towish').click(function(){
	$(this).toggleClass('hidden');
    $(this).parent().find('.fromwish').toggleClass('hidden');

});
$('.fromwish').click(function(){
    $(this).toggleClass('hidden');
    $(this).parent().find('.towish').toggleClass('hidden');

});
function towishlist(e, id, url) {
    e.preventDefault();

    $.get(url + "main/addtowishlist", {id: id}, function (data) {
        var data = $.parseJSON(data);
        //console.log(data);
        if (data.flag == 1) {
            $("#added").modal("show");
        }
        $('.favorite .quantity').html(parseInt($('.cart_header .favorite .quantity').text()) + 1);
    });
}
function delfromwishlist(e, id, url) {
    e.preventDefault();
    
    $.get(url + 'main/delfromwishlist', {id: id}, function (data) {
        var data = $.parseJSON(data);
        if (data.flag == 1) {
            $(".prod" + id).css("display", "none");
            $(".wish_h4 span").text(parseInt($(".wish_h4 span").text()) - 1);
        }
        $('.favorite .quantity').html(parseInt($('.cart_header .favorite .quantity').text()) - 1);
    });
}
$(".soc_group").on("click", function (e) {
    if ($(this).attr("href") == "") {
        e.preventDefault();
        $("#socmod").modal("show");
    }
});
$(".contact_soc a").on("click", function (e) {
    if ($(this).attr("href") == "") {
        e.preventDefault();
        $("#socmod").modal("show");
    }
});
function indexTab(cls){
    $('.indexTab').css('height','0px');
    $('.'+cls).css('height','auto');

}
function Menu(){
    var top = $(document).scrollTop();
    if (top < 260) {
        $(".main_menu").removeClass('float');                            
    }               
    else {
        $(".main_menu").addClass('float');                       
    }
}

