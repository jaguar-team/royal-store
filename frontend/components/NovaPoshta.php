<?php

namespace frontend\components;
use Yii;

class NovaPoshta
{
    public $url;
    public function __construct()
    {
        $this->url='https://api.novaposhta.ua/v2.0/json/';
    }
    public function getCityes()
    {
        $data = array(
        			'apiKey' => 'a4e54589af0f12dd041c57d073a119b1',
        			'modelName' => 'Address',
        			'calledMethod' => 'getCities',
        		);
        $post = json_encode($data);
        $result = json_decode(file_get_contents($this->url, null, stream_context_create(array(
    		    'http' => array(
    		        'method' => 'POST',
    		        'header' => "Content-type: application/x-www-form-urlencoded;\r\n",
    		        'content' => $post,
    		    ),
    		))), true);
        foreach($result['data'] as $res)
            $cityes[$res['Description']]=$res['DescriptionRu'];
        
        return $cityes;
    }
    public function getOffices($city)
    {
        $data = array(
        			'apiKey' => 'a4e54589af0f12dd041c57d073a119b1',
        			'modelName' => 'Address',
        			'calledMethod' => 'getWarehouses',
                    'methodProperties'=>[
                        "CityName"=> $city,
                    ]
        		);
        $post = json_encode($data);
        $result = json_decode(file_get_contents($this->url, null, stream_context_create(array(
    		    'http' => array(
    		        'method' => 'POST',
    		        'header' => "Content-type: application/x-www-form-urlencoded;\r\n",
    		        'content' => $post,
    		    ),
    		))), true);
        foreach($result['data'] as $res)
            $offices[$res['Description']]=$res['DescriptionRu'];
            
        return $offices;
    }
}