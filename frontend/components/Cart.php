<?php
    
    namespace frontend\components;  
    
    use common\models\Product;
    use Yii;
    
    class Cart 
    {
        public $ids;
        
        public function __construct()
        {
             $this->ids=Yii::$app->session->get('ids');
        }
        
        public function addToCart($id, $count=1, $size)
        {
            if($this->ids)
            {
                $is=false;
                $i=0;
                foreach($this->ids as $ids)
                {
                    if($ids['id']==$id && $ids['size']==$size)
                    {
                        $this->ids[$i]['count']+=$count;
                        $is=true;
                        break;
                    }
                    $i++;
                }
                if($is==false)
                {
                    $this->ids[]=['id'=>$id, 'count'=>$count, 'size'=>$size];
                }
            }
            else
            {
                $this->ids=[0=>['id'=>$id, 'count'=>$count, 'size'=>$size]];
            }
                
            
            Yii::$app->session->set('ids', $this->ids);
            Yii::$app->session->set('count', Yii::$app->session->get('count')+$count);
            
            Yii::$app->session->set('price', Yii::$app->session->get('price')+Product::find()->where(['id'=>$id])->one()->r_price);
            return true;

        }
        public function delFromCart($id, $size)
        {               
            $i=0;
            $arr=[];
            foreach($this->ids as $ids)
            {
                if($ids['id']==$id && $ids['size']==$size)
                {
                    $count_el=$ids['count'];
                    /*unset($this->ids[$i]);
                    break;*/
                }
                else
                {
                    $arr[]=['id'=>$ids['id'], 'count'=>$ids['count'], 'size'=>$ids['size']];
                }
                $i++;
            }

            Yii::$app->session->set('ids', $arr);
            Yii::$app->session->set('count', Yii::$app->session->get('count')-$count_el);
            
            Yii::$app->session->set('price', Yii::$app->session->get('price')-Product::find()->where(['id'=>$id])->one()->r_price*$count_el);
        }
        
        public function minusCart($id, $size)
        {   
            $i=0;
            foreach($this->ids as $ids)
            {
                if($ids['id']==$id && $ids['size']==$size)
                {
                    if($ids['count']>=2)
                    {
                        $this->ids[$i]['count']--;
                        Yii::$app->session->set('count', Yii::$app->session->get('count')-1);
                        break;
                    }
                }
                $i++;
            }

            Yii::$app->session->set('ids', $this->ids);
            Yii::$app->session->set('price', Yii::$app->session->get('price')-Product::find()->where(['id'=>$id])->one()->r_price);
            //$this->price();
            return true;
        }
        public function plusCart($id, $size)
        {   
            $i=0;
            foreach($this->ids as $ids)
            {
                if($ids['id']==$id  && $ids['size']==$size)
                {
                    $this->ids[$i]['count']++;
                    Yii::$app->session->set('count', Yii::$app->session->get('count')+1);
                    break;
                }
                $i++;
            }

            Yii::$app->session->set('ids', $this->ids);

            $this->price();
            return true;
        }
        
        /*public function recalc($ids, $post_counts)
        {
            $new_id_count=array();
            $new_ids=array();
            $count=0;
            $j=0;
            for($i=0;$i<count($ids);$i++)
            {
                if($post_counts[$i]>0 && $post_counts[$i]<1000 && preg_match('/^\+?\d+$/', $post_counts[$i]))
                {
                    $new_id_count[$j]=$post_counts[$i];
                    $new_ids[$j]=$ids[$i];
                    $count+=$post_counts[$i];
                    $j++;
                }
            }
            
            Yii::$app->session->set('ids', $new_ids);
            Yii::$app->session->set('id_count', $new_id_count);
            Yii::$app->session->set('count', $count);
                
            $this->price($new_ids, $new_id_count);
        }*/
        public function price()
        {   
            $products=array();
            for($i=0;$i<count($this->ids);$i++)
                $products[]=Product::find()->where(['id'=>$this->ids[$i]['id']])->one();
                
            $price=0;
            for($i=0;$i<count($this->ids);$i++)
            {
                $price+=$products[$i]->r_price*$this->ids[$i]['id'];
            }
            
            Yii::$app->session->set('price', $price);
            return true;
        }
        
    }

